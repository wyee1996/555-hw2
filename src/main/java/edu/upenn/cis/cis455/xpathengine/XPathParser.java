package edu.upenn.cis.cis455.xpathengine;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XPathParser {

	String queryId;
	
	PathNode root;
	PathNode prevNode;
	
	public XPathParser(Integer queryId) {
		this.queryId = queryId.toString();
	}
	
	/**
	 * Parses an XPath expression and creates a series of path nodes 
	 * @param expression
	 */
	public PathNode parse(String expression) throws InvalidXPathException{

        // Get method
        if (expression.isEmpty()) {
        	throw new InvalidXPathException("XPath expression is empty");
        }
        	
        	// all XPaths should begin with a /
        	if (expression.charAt(0) != '/') {
        		throw new InvalidXPathException("XPath expression must begin with a /");
        	}
        	
        	int position = 1;
        	PathNode currPathNode = new PathNode(this.queryId, position);
        	root = currPathNode;
        	root.setRoot(root);
        	String currPathElement = currPathNode.getElementString();
        	boolean insideQuotes = false;
        	boolean hasSpaces = false;
        	for(int i = 1; i < expression.length(); i++) {
        		
        		Character currChar = expression.charAt(i);
        		
        		if (currChar.equals(' ')) hasSpaces = true;
        		
        		// once we've hit a /, want to check if we're inside or outside quotes to break out
        		if (currChar == '/') {
        			// if we're not inside quotes, then break out
        			if (!insideQuotes) {
        				currPathNode.setElementString(currPathElement);
        				// if there are spaces, then remove them
        				if (hasSpaces) {
        					// if the spaces aren't valid, then return null
        					removeSpace(currPathNode);
        				}else {
        					handleNodeName(currPathNode);
        					handleBracketExpressions(currPathNode);
        				}
        				addNodeToPath(currPathNode);
        				position++;
            			currPathNode = new PathNode(this.queryId, position);
            			currPathNode.setRoot(root);
            			currPathElement = currPathNode.getElementString();
            			continue;
        			}        			
        		}
        		
        		// check if we've hit a quote
        		if (currChar == '\"') {
        			// check to see if current quote has an escape char before it
        			if (expression.charAt(i-1) == '\\') {
        				// if so, then continue b/c this don't want this to affect insideQuotes
        				continue;
        			}
        			// otherwise, this means we want to flip current status of insideQuotes
        			insideQuotes = !insideQuotes;
        		}
        		
    			currPathElement += currChar;
        		
        	}
        	
        	currPathNode.setElementString(currPathElement);
        	if (hasSpaces) {
        		removeSpace(currPathNode);
        	}
        	else {
        		handleNodeName(currPathNode);
        		handleBracketExpressions(currPathNode);
        	}
        	addNodeToPath(currPathNode);
        	
        	return root;
	}
	
	/**
	 * Removes whitespaces from string element
	 * @param node
	 */
	public void removeSpace(PathNode node) throws InvalidXPathException {
				
		String element = node.getElementString();
		
		handleNodeName(node);
		
		handleBracketExpressions(node);
		
		// remove all whitespaces from the element text if good
		String trimmedElement = element.replaceAll("\\s+", "");
		node.setElementString(trimmedElement);
		
		if (trimmedElement.isEmpty()) throw new InvalidXPathException("XPath can't end with a /");
		
	}
	
	public void addNodeToPath(PathNode node) {
		if (prevNode == null) {
			prevNode = node;
		}
		else {
			prevNode.setChild(node);
			node.setParent(prevNode);
			prevNode = node;
		}
	}
	
	public void handleNodeName(PathNode node) throws InvalidXPathException {
		
		String element = node.getElementString();
		
		// first get the nodename
		String nodeName = "";
		for (int i=0; i<element.length(); i++) {
			Character currChar = element.charAt(i);
			if (currChar == '[') {
				break;
			}
			if (Character.isLetterOrDigit(currChar)) {
				nodeName += currChar;
			}
		}
		nodeName.trim();
		// if after trimming, nodeName is empty, then it's not a valid element
		if (nodeName.isEmpty()) throw new InvalidXPathException("XPath does not contain a node name");
		node.setNodeName(nodeName);
	}
	
	public void handleBracketExpressions(PathNode node) throws InvalidXPathException {
		
		String element = node.getElementString();
		
		// now get stuff inside brackets
		Pattern pattern = Pattern.compile("\\[(.*?)\\]");
		Matcher matcher = pattern.matcher(element);
		while (matcher.find()) {
			String currBrack = matcher.group();
			
			// check if contains is in it or not (w/ or w/out whitespace)
			// if it is, then ensure that it is continuous. if not then return false
			Pattern containPattern = Pattern.compile("c(?:\\s*)o(?:\\s*)n(?:\\s*)t(?:\\s*)a(?:\\s*)i(?:\\s*)n(?:\\s*)s");
			Matcher containMatch = containPattern.matcher(currBrack);
			if (containMatch.find()) {
				if (!currBrack.contains("contains")) {
					throw new InvalidXPathException("\"contains\" must not contain spaces");
				}
				
				// add this bracket expression to the path node.				
				node.addToBracketText(parseBracketText(currBrack,"contains"), "contains");
				continue;
			}
			
			// if doesn't have continuous word text in it, then return false
			if (!currBrack.contains("text")) {
				throw new InvalidXPathException("bracket condition must contain "
						+ "\"text\" and must not have spaces within \"text\"");
			}
			else {
				node.addToBracketText(parseBracketText(currBrack,"text"), "text");
			}
		}
	}
	
	public String parseBracketText(String bracketExp, String containsOrText) throws InvalidXPathException {
		bracketExp = bracketExp.replaceAll("\\s+", "");
		String parsedBrackExp = "";
		int brackExpLength = bracketExp.length();
		if (containsOrText.equals("contains")) {
			if (bracketExp.substring(0, 18).equals("[contains(text(),\"") && bracketExp.substring(brackExpLength - 3).equals("\")]")) {
				parsedBrackExp = bracketExp.substring(18,brackExpLength-3);
			}
			else {
				throw new InvalidXPathException("contains(text(),\"...\") bracket expression is invalid");
			}
		}
		else if (containsOrText.equals("text")) {
			if (bracketExp.substring(0,9).equals("[text()=\"") && bracketExp.substring(brackExpLength - 2).equals("\"]")){
				parsedBrackExp = bracketExp.substring(9,brackExpLength - 2);
			}
			else {
				throw new InvalidXPathException("text()=\"...\" bracket expression is invalid");
			}
		}
		return parsedBrackExp;
	}
}
