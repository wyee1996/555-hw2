package edu.upenn.cis.cis455.xpathengine;

public class InvalidXPathException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidXPathException(String message) {
		super(message);
	}
	
	public InvalidXPathException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public InvalidXPathException(Throwable cause) {
		super(cause);
	}
}
