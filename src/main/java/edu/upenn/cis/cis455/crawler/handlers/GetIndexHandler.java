package edu.upenn.cis.cis455.crawler.handlers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.storage.dataAccessors.UserDataAccessor;
import edu.upenn.cis.cis455.storage.databaseDAOs.UserDAO;
import edu.upenn.cis.cis455.webinterface.BuildIndexPage;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Session;
import static spark.Spark.*;

public class GetIndexHandler implements Route{
	final static Logger logger = LogManager.getLogger(GetIndexHandler.class);

	StorageInterface db;
	
	public GetIndexHandler(StorageInterface db) {
		this.db = db;
	}
	
	@Override
	public Object handle(Request request, Response response) throws Exception {
		
		logger.info("Getting session from request");
		Session session = request.session(false);
		
		if (session == null) {
			logger.info("There's no session for this request. Redirecting to the login page!");
			response.redirect("/login-form-invalid.html");
			halt();
		}
		String username = session.attribute("user");
		logger.info("The username tied to this session is "+username);
		
		UserDataAccessor userDataAccess = new UserDataAccessor(db.getEntityStore());
		UserDAO user = userDataAccess.getUserForUsername(username);
		
		BuildIndexPage buildIndex = new BuildIndexPage(db);
		
		return buildIndex.create(user);
	}

}
