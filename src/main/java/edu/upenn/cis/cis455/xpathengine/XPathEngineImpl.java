package edu.upenn.cis.cis455.xpathengine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Set;

import edu.upenn.cis.cis455.xpathengine.OccurrenceEvent.Type;

public class XPathEngineImpl implements XPathEngine{

	String[] _expressions;
	
	Hashtable<PathNode, Integer> pathsTable = new Hashtable<PathNode, Integer>();
	
	QueryIndex queryIndex = new QueryIndex();
	
	boolean[] pathStatus;	
	public XPathEngineImpl() {}
	
	@Override
	public void setXPaths(String[] expressions) {
		this._expressions = expressions;
		pathStatus = new boolean[_expressions.length];
		Arrays.fill(pathStatus, false);
		int count = 0;
		for (String expression : expressions) {
			XPathParser parser = new XPathParser(count);
			PathNode newPath;
			try {
				newPath = parser.parse(expression);
				pathsTable.put(newPath, count);
				addPathToQueryIndex(newPath);
				queryIndex.printQueryIndex("Register XPaths");
			} catch (InvalidXPathException e) {
				e.printStackTrace();
			}
			count++;
		}
	}

	@Override
	public boolean isValid(int i) {
		return pathStatus[i];
	}

	@Override
	public boolean[] evaluateEvent(OccurrenceEvent event) {
		// OccurrenceEvent has a document ID and a "parse event"
		
		// get the event value and event type
		Type type = event.getType();
		String value = event.getValue();
		int depth = event.getDepth();
		
		// handle open event
		if (type.equals(OccurrenceEvent.Type.Open)) {
			handleOpenEvent(value, depth);
		}
		
		// handle text event
		if (type.equals(OccurrenceEvent.Type.Text)) {
			handleTextEvent(value, depth, event.getEncapsulatingElement());
		}
				
		// handle close event
		if (type.equals(OccurrenceEvent.Type.Close)) {
			handleCloseEvent(value, depth);
		}
		
		//queryIndex.printQueryIndex("handle event "+event.toString());
		return pathStatus;
		
	}
	
	
	
	private void handleOpenEvent(String elementStr, int position) {
		
		// access the queryIndex at the given value
		ArrayList<PathNode> candList = queryIndex.getCandList(elementStr);
		
		ArrayList<PathNode> removeNodes = new ArrayList<PathNode>();
		
		ArrayList<PathNode> successNodes = new ArrayList<PathNode>();
		
		ArrayList<PathNode> promoteNodes = new ArrayList<PathNode>();
		
		// loop thru each node in candidate list. 
		for (PathNode node : candList) {
			
			// for each node that has value = nodeName & position = depth
			if (node.getNodeName().equals(elementStr) && node.getPosition() == position) {
				
				// if the path node has no child and also doesn't have bracket text conditions,
				// then we are at the final path node of query so we get a match
				if (node.getChild() == null && node.getBracketText().isEmpty()) {
					successNodes.add(node.getRoot());
				}
				
				// promote the child
				if (node.getChild() != null) {
					promoteNodes.add(node.getChild());
				}
			}
//			else {
//				removeNodes.add(node.getRoot());
//			}
		}
		
		// remove these paths from the query index b/c this saves having to iterate through them in future queries
		// if a path has failed, then remove it
		removePathsFromQueryIndex(removeNodes, false);
		// if a path has matched, then remove it
		removePathsFromQueryIndex(successNodes, true);
		promoteQueryIndexes(promoteNodes);
		
	}
	
	private void handleTextEvent(String textStr, int position, String encapsulatingElement) {
		
		// access the queryIndex at the given value
		ArrayList<PathNode> candList = queryIndex.getCandList(encapsulatingElement);
		
		ArrayList<PathNode> removeNodes = new ArrayList<PathNode>();
		
		ArrayList<PathNode> successNodes = new ArrayList<PathNode>();
				
		// loop thru each node in candidate list. 
		for (PathNode node : candList) {
			
			// for each node that has value = nodeName & position = depth
			if (node.getNodeName().equals(encapsulatingElement) && node.getPosition() == position) {
				
				boolean success = true;
				
				success = checkBrackets(textStr, node, success);
				
				if (success == true && node.getChild() == null) {
					successNodes.add(node.getRoot());
				}
				
				// if we fail any of the bracket scenarios, then set the path status to false
				if (success == false) removeNodes.add(node.getRoot());
				
				// if there aren't any bracket conditions, then continue b/c we've already promoted the
				// encapsulating element.
			}
		}
		
		removePathsFromQueryIndex(removeNodes, false);
		
		removePathsFromQueryIndex(successNodes, true);
	}

	
	
	private void handleCloseEvent(String elementStr, int position) {
		ArrayList<PathNode> candList = queryIndex.getCandList(elementStr);
		
		for (PathNode node : candList) {
			if (node.getNodeName().equals(elementStr) && node.getPosition() == position) {
				
				if (node.getChild() != null) {
					// remove the child node from its candidate list
					queryIndex.deleteFromCandList(node.getChild().getNodeName(), node.getChild());
				}
			}
		}
	}

	private void promoteQueryIndexes(ArrayList<PathNode> nodes) {
		for (PathNode node : nodes) {
			// promote a given node from waitlist to candidate list
			queryIndex.promote(node.getNodeName(), node);
		}
	}
	
	private boolean checkBrackets(String textStr, PathNode node, boolean success) {
		// loop thru all of the bracket items and check if true or not
		Hashtable<String, String> bracketText = node.getBracketText();
		Set<String> bracketSet = bracketText.keySet();
		for (String text : bracketSet) {
			
			// if at any point we don't meet one of the bracket expressions, then break
			if (success == false) break;
			String containsOrText = bracketText.get(text);
			
			// if bracket text is "contains", then we want to check if node's text is inside textStr
			if (containsOrText.equals("contains")) {
				if (textStr.contains(text)) success = true;
				else success = false;
			}
			// otherwise if bracket text is "text", check if node's text is same as textStr
			else if (containsOrText.equals("text")) {
				if (textStr.equals(text)) success = true;
				else success = false;
			}
		}
		return success;
	}

	/**
	 * Sets the bit status of a given path
	 * Also removes nodes in all candidate or waitlists with root node+descendents if status = false
	 * @param node
	 * @param status
	 */
	private void setPathStatus(PathNode node, boolean status) {
		
		// get the root node of path & find the index of it in expressions array from pathsTable
		PathNode root = node.getRoot();
		int i = pathsTable.get(root);
		
		// set the index in expressions array = true
		// means this xpath is still a good candidate
		pathStatus[i] = status;
	}
	
	private void addPathToQueryIndex(PathNode node) {
		while(node != null) {
			if (node.getPosition() == 1) {
				queryIndex.addToCandidateList(node.getNodeName(), node);
				node = node.getChild();
			}
			else {
				queryIndex.addToWaitList(node.getNodeName(), node);
				node = node.getChild();
			}
		}
	}
	
	/**
	 * Removes paths from the query index. 
	 * If the path has matched, then isSuccess is true and we set path status as true for that path
	 * @param nodeList
	 * @param isSuccess
	 */
	private void removePathsFromQueryIndex(ArrayList<PathNode> nodeList, boolean isSuccess) {
		for (PathNode node : nodeList) {
			if (isSuccess) {
				System.out.println("\nPath "+node.getQueryId()+" matched. Removing from query index!");
			}
			else {
				System.out.println("\nPath "+node.getQueryId()+" failed. Removing from query index!");
			}
			setPathStatus(node, isSuccess);
			while(node != null) {
				queryIndex.deleteFromCandList(node.getNodeName(), node);
				queryIndex.deleteFromWaitList(node.getNodeName(), node);
				node = node.getChild();
			}
			
		}
	}
	
	public void printPathStatus() {
		int i = 0;
		for (boolean bool : pathStatus) {
			System.out.println("Expression: "+i+" - Status: "+bool);
			i++;
		}
	}
	
//	private void convertToBoolArray(Hashtable<Integer, PathNode> pathsTable) {
//		boolean[] newPathStatus = new boolean[_expressions.length];
//		Set<Integer> iter = pathsTable.keySet();
//		for (Integer i : iter) {
//			newPathStatus[i] = true;
//		}
//		
//		pathStatus = newPathStatus;
//	}

}
