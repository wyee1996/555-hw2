package edu.upenn.cis.cis455.xpathengine;

import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.tuple.Values;

public class SAXParseHandler extends DefaultHandler{
	
	private OutputCollector collector;
	
	public SAXParseHandler(OutputCollector collector) {
		super();
		this.collector = collector;
	}
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		createOccurrenceEvent("start",qName);
	}
	
	@Override
    public void characters(char[] ch, int start, int length) throws SAXException {
		String str = new String(ch,start, length);
		createOccurrenceEvent("char",str);
	}
	
	@Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
		createOccurrenceEvent("end",qName);		
    }
	
	/**
	 * Spits out an occurrence event
	 * @param type
	 * @param val
	 * @return
	 */
	public void createOccurrenceEvent(String type, String val) {
//		if (type == "end") {
//			collector.emit(new Values<Object>(new OccurrenceEvent(OccurrenceEvent.Type.Close, val)));
//		}
//		else if (type == "start") {
//			collector.emit(new Values<Object>(new OccurrenceEvent(OccurrenceEvent.Type.Open, val)));
//		}
//		else if (type == "char") {
//			collector.emit(new Values<Object>(new OccurrenceEvent(OccurrenceEvent.Type.Text, val)));
//		}
	}
	
}
