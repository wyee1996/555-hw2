package edu.upenn.cis.cis455.xpathengine;

public class DocumentInfo {

	String documentBody;
	
	String url;
	
	String type;
	
	String hostname;
	
	public DocumentInfo(String docbody, String url, String type, String hostname) {
		this.documentBody = docbody;
		this.url = url;
		this.type = type;
		this.hostname = hostname;
	}
	
	public String getDocumentBody() {
		return this.documentBody;
	}
	
	public String getUrl() {
		return this.url;
	}
	
	public String getType() {
		return this.type;
	}
	
	public String getHostname() {
		return this.hostname;
	}
	
	public void setDocumentBody(String docBody) {
		this.documentBody = docBody;
	}
	
}
