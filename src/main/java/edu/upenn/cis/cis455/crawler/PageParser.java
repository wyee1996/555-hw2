package edu.upenn.cis.cis455.crawler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map.Entry;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Connection.Method;
import org.jsoup.Jsoup;
import org.jsoup.Connection.Response;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


import edu.upenn.cis.cis455.crawler.utils.URLInfo;

public class PageParser {
	static final Logger logger = LogManager.getLogger(PageParser.class);

	static int max_length = 5000;
	String CRLF = "\r\n";
	
	/**
	 * Check the url header to ensure it's correct content type
	 * and also within max length
	 * Returns true if it's good.
	 * @param url
	 * @return
	 * @throws IOException
	 */
	public static boolean checkHead(URLInfo url) throws IOException {
		//logger.info("Sending HEAD request to "+url.toString());
		Response response = Jsoup.connect(url.toString()).
							userAgent("cis455crawler").
							cookie("auth","token").
							timeout(3000).
							method(Method.HEAD).
							execute();
		
		try {
			Integer length = Integer.parseInt(response.header("Content-Length"));
			//logger.info("Content-Length: "+length.toString());
			if (length > max_length) {
				return false;
			}			
		}catch(Exception ex) {
			// Continue if content length is null
		}
		
//		for (Entry<String,String> header : response.headers().entrySet()){
//			System.out.println(header.getKey() + ": "+header.getValue());
//		}
		
		String contentType = response.header("Content-Type");
		//logger.info("Content-Type: "+contentType);
		if (contentType.contains("text/plain") || contentType.contains("text/html")
				|| contentType.endsWith("xml")) {
			url.setContentType(contentType);
			return true;
		}
		return false;
	}
	
	/**
	 * Gets absolute urls from page and stores them into list of URLInfo
	 * @param url
	 * @return
	 * @throws IOException
	 */
	public static ArrayList<URLInfo> parsePage(String hostname, String documentBody) throws IOException{
//		Document doc = Jsoup.connect(url).
//						userAgent("cis455crawler").
//						cookie("auth","token").
//						timeout(3000).
//						get();
		
		
		ArrayList<URLInfo> linksInPage = new ArrayList<URLInfo>();
		
		Document doc = Jsoup.parse(documentBody, hostname);
		
		Elements elements = doc.select("a[href]");
		for (Element element : elements) {
			String absUrl = element.attr("abs:href");
			URLInfo urlinfo = new URLInfo(absUrl);
			linksInPage.add(urlinfo);
		}
		
		//Pair<ArrayList<URLInfo>,String> pair = Pair.of(linksInPage, documentBody);
		
		return linksInPage;
	}
	
	public static String downloadDocumentFromUrl(String url) throws IOException {
		URL Url = new URL(url);
		HttpURLConnection urlConnection = (HttpURLConnection)Url.openConnection();
		
		//String userCreds = ""
		urlConnection.setRequestMethod("GET");
		urlConnection.setRequestProperty("User-Agent", "cis455crawler");
		urlConnection.setUseCaches(false);
		urlConnection.setDoInput(true);
		urlConnection.setDoOutput(true);
		
		return readStream(urlConnection.getInputStream());
	}
	
	public static String readStream(InputStream in) {
//		StringBuilder sb = new StringBuilder();
//		try (BufferedReader reader = new BufferedReader(new InputStreamReader(in));){
//			String nextLine = "";
//			while((nextLine = reader.readLine()) != null) {
//				sb.append(nextLine);
//			}
//		} catch(IOException e) {
//			
//		}
//		return sb.toString();
		String content_body = "";
		int charac = 0;
		try {
			while((charac = in.read()) != -1) {
				content_body += (char)charac;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return content_body;
		
	}
	
}
