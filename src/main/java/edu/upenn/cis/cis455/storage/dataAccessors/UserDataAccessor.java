package edu.upenn.cis.cis455.storage.dataAccessors;

import com.sleepycat.je.DatabaseException;
import com.sleepycat.persist.EntityCursor;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.SecondaryIndex;

import static spark.Spark.*;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.storage.DatabaseEnvironment;
import edu.upenn.cis.cis455.storage.databaseDAOs.ChannelDAO;
import edu.upenn.cis.cis455.storage.databaseDAOs.UserDAO;

public class UserDataAccessor {
	final static Logger logger = LogManager.getLogger(UserDataAccessor.class);

	// User accessor
	private PrimaryIndex<String,UserDAO> userByUsername;
	
	
	private SecondaryIndex<String, String, UserDAO> usersByChannel;
	// Secondary index
	//private SecondaryIndex<String,String,UserDAO> userBySessionId;
		
	EntityStore store;
	
	public UserDataAccessor(EntityStore store) {
		
		this.store = store;
		
		// primary key for user class
		userByUsername = store.getPrimaryIndex(String.class, UserDAO.class);	
		
		usersByChannel = store.getSecondaryIndex(userByUsername, String.class, "channels");
		
	}
	
		
	public UserDAO getUserForUsername(String username) throws DatabaseException {
		return userByUsername.get(username);
	}
	
	
	public UserDAO getUserForUsernameCheckPass(String username, String password) throws DatabaseException, NoSuchAlgorithmException {
		UserDAO user = getUserForUsername(username);
		if (user == null) {
			return null;
		}
		String encryptedPass = encryptPassword(password);
		logger.info("Inputted cleartext pass is "+password);
		logger.info("Inputted encrypted pass is "+encryptedPass);
		String storedEncryptedPass = user.getPassword();
		logger.info("Stored encrypted pass is "+storedEncryptedPass);
		if(storedEncryptedPass.equals(encryptedPass)) {
			return user;
		}
		return null;
	}
	
	/**
	 * Updates a user and any parameters that have changed
	 * @param user
	 * @throws DatabaseException
	 */
	public void updateUser(UserDAO user) throws DatabaseException {
		userByUsername.put(user);
	}
	
	
	// TODO: ADD ENCRYPTION FUNCTIONALITY
	/**
	 * @param password
	 * @return
	 * @throws NoSuchAlgorithmException 
	 */
	public String encryptPassword(String password) throws NoSuchAlgorithmException {
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		byte[] hash = digest.digest(password.getBytes(StandardCharsets.UTF_8));
		String hashString = Base64.getEncoder().encodeToString(hash);
		//String hashString = org.apache.commons.digest.DigestUtils.sha256Hex(password);
		return hashString;
	}
	
	public void printAllUsers() {
		EntityCursor<UserDAO> users = userByUsername.entities();
		System.out.println("Printing all users");
		for (UserDAO user : users) {
			System.out.println("User: "+user.getUsername()+" - Pass: "+user.getPassword());
		}
	}
}
