package edu.upenn.cis.cis455.storage.dataAccessors;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sleepycat.persist.EntityCursor;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.SecondaryIndex;

import edu.upenn.cis.cis455.crawler.RobotsTxtHandler;
import edu.upenn.cis.cis455.storage.databaseDAOs.UserDAO;
import edu.upenn.cis.cis455.storage.databaseDAOs.WebPageDAO;

public class WebPageDataAccessor {
	static final Logger logger = LogManager.getLogger(WebPageDataAccessor.class);

	// Webpage primaryindex accessor
	public PrimaryIndex<Integer,WebPageDAO> webPageByHash;
	
	// Webpage secondaryindex accessor
	public SecondaryIndex<String,Integer, WebPageDAO> webPageByUrl;
	
	public WebPageDataAccessor(EntityStore store) {
		// primary key for user class
		webPageByHash = store.getPrimaryIndex(Integer.class, WebPageDAO.class);	
		webPageByUrl = store.getSecondaryIndex(webPageByHash,String.class,"urls");
	}
		
	/**
	 * Converts a given webpage from string to bytes
	 * @param page
	 */
	public byte[] ConvertPageContentToBytes(String pageContent) {
		return pageContent.getBytes();
	}
	
	/**
	 * Adds a new web page and corresponding link to the database if it doesn't exist.
	 * If it web page content does exist, then add it to 
	 * @param url
	 * @param webpage
	 */
	public void addDocument(String pageContent, String url, String contentType) {
		
		System.out.println(pageContent.hashCode());
		WebPageDAO webpage = webPageByHash.get(pageContent.hashCode());
		if (webpage != null) {
			logger.info(url+": already indexed this page");
			webpage.addLinkToWebPage(url);
			webPageByHash.put(webpage);
		}
		else {
			webpage = new WebPageDAO(pageContent, url, contentType);
			webpage.setTimeLastChecked(new Date());
			webPageByHash.put(webpage);
		}
	}
	
	public WebPageDAO getWebPageForHash(int hash) {
		return webPageByHash.get(hash);
	}
	
	public WebPageDAO getWebPageForUrl(String url){
		WebPageDAO webpage = webPageByUrl.get(url);
		return webpage;
	}
	
	public void printAllWebPages() {
		EntityCursor<WebPageDAO> webpages = webPageByHash.entities();
		System.out.println("Printing all web pages");
		for (WebPageDAO webpage : webpages) {
			System.out.println("Webpage Content: "+webpage.getWebPageContents().substring(0, 10)+" - Urls: "+webpage.getLinks()+" - Checked Time: "+webpage.getTimeLastChecked());
		}
	}
	
}
