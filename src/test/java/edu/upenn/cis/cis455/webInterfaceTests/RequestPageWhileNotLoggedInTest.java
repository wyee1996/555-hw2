package edu.upenn.cis.cis455.webInterfaceTests;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

import edu.upenn.cis.cis455.databaseTests.SetupDatabase;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;

/**
 * Tests if the user is not logged in but tries to request a page
 * These tests assume that the request is valid
 * @author vagrant
 *
 */
public class RequestPageWhileNotLoggedInTest {	
	
	String homeDirectory = "/vagrant/testDatabase";
	StorageInterface database;
	
//	@BeforeClass
//	public void before() throws IOException {
//		SetupDatabase.purgeDatabase(homeDirectory);
//		database = StorageFactory.getDatabaseInstance(homeDirectory, false);
//		database.addUser("testUser", "pass123");
//	}
	
	// Test1: User is not logged in and doesn't have a session Id cookie
			// Output: Returns the login page
	@Test
	public void test1() {
		
	}
	
	// Test2: User is not logged in and makes request to /login
			// Returns the login page
	// Test3: User is not logged in and makes request to /register
			// Returns the register page
	// Test4: User is not logged in and makes request to any other page
			// Returns the login page

}
