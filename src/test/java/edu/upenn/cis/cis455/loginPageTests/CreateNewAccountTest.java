package edu.upenn.cis.cis455.loginPageTests;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.upenn.cis.cis455.crawler.WebInterface;
import edu.upenn.cis.cis455.databaseTests.MockSocket;
import edu.upenn.cis.cis455.databaseTests.SetupDatabase;
import edu.upenn.cis.cis455.databaseTests.TestHelper;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.storage.dataAccessors.SessionDataAccessor;
import edu.upenn.cis.cis455.storage.dataAccessors.UserDataAccessor;

/**
 * Test that can create a new account
 * @author vagrant
 *
 */
public class CreateNewAccountTest {

		
	final static Logger logger = LogManager.getLogger(CreateNewAccountTest.class);
	
	StorageInterface database;
	SessionDataAccessor sessionDataAccessor;
	UserDataAccessor userDataAccessor;
	
	String request = "POST /register?username=testUser&password=testPass123 HTTP/1.1";
	
	@BeforeClass
	public static void setup() throws IOException {
		org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
		logger.info("Starting CreateNewAccountTest suite");
		//SetupDatabase.purgeDatabase("/database");	
	}
	
	@Test
	public void test1() throws IOException {
//		String[] args = {"/database","/vagrant/555-hw2/www"};
//		WebInterface.main(args);
	}

}
