package edu.upenn.cis.cis455.crawler;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.crawler.utils.RobotsTxtInfo;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.storage.dataAccessors.WebPageDataAccessor;

public class CrawlerWorker extends Thread{
	static final Logger logger = LogManager.getLogger(CrawlerWorker.class);
	
	private final Crawler crawler;
	RobotsTxtInfo robotsInfo;
	int thread;
	
	public CrawlerWorker(Crawler crawler, int thread) {
		this.crawler = crawler;
		this.thread = thread;
		logger.info("CrawlerWorker "+thread+" started");
	}
	
	@Override
    public void run() {
	
//		while(!crawler.isDone()) {
//		
//			try {
//				
//				// get top link from the linkQueue
//				URLInfo link = crawler.linkQueue.popLink();
//				
//				crawler.setWorking(true, thread);
//				
//				// check if the link's hostname already has a robots.txt
//				// if it doesn't, then add it to robotsForHostname
//				if (!Crawler.robotsForHostname.containsKey(link.getHostName())) {
//					
//					// check if link has a robots.txt
//					BufferedReader reader = RobotsTxtHandler.getRobotsTxt(link.toString()+"/robots.txt");
//					if (reader != null) {
//						robotsInfo = RobotsTxtHandler.parse(reader);
//					}
//					else {
//						// if doesn't exist, then no restrictions
//						robotsInfo = new RobotsTxtInfo();
//					}
//					
//					Crawler.robotsForHostname.put(link.getHostName(), robotsInfo);
//				}
//				else {
//					robotsInfo = Crawler.robotsForHostname.get(link.getHostName());
//				}
//				
//				// make sure that crawler is allowed crawl the link. If not, then skip it
//				if (crawler.isOKtoCrawl(link)) {
//					crawler.notifyThreadExited(thread);
//					continue;
//				}
//				
//				// make sure that we are following crawl delay directive. 
//				Integer crawlDelay = crawler.deferCrawl(link);
//				if (crawlDelay != null) {
//					Thread.sleep(crawlDelay*1000);
//				}
//				
//				logger.info("CrawlerWorker "+thread+" crawling link " + link.toString());
//				
//				// send HEAD request to ensure content type and length are within our bounds
//				if (!PageParser.checkHead(link)) {
//					crawler.notifyThreadExited(thread);
//					continue;
//				}
//	    	
//				// for each url, parse content and download link
//    			
//				Pair<ArrayList<URLInfo>, String> parsePagePair = PageParser.parsePage(link.toString());
//				String documentBody = parsePagePair.getRight();
//				if (documentBody.isEmpty()) {
//					crawler.notifyThreadExited(thread);
//					continue;
//				}
//				
//				ArrayList<URLInfo> linksInPage = parsePagePair.getLeft();
//				
//				// If document is not empty, check if we've already indexed it
//				WebPageDataAccessor webPageDataAccessor = new WebPageDataAccessor(crawler.db.getEntityStore());
//				if (webPageDataAccessor.getWebPageForHash(documentBody) != null) {
//					crawler.notifyThreadExited(thread);
//					continue;
//				}
//				
//				// If we haven't indexed it, then add document body to DB
//				webPageDataAccessor.addDocument(documentBody, link.toString(), link.getContentType());
//				logger.info(link.toString() + ": downloading");
//				crawler.incCount();
//				
//				// extend crawl delay time after we've crawled it
//				RobotsTxtHandler.extendCrawlDelayTime(robotsInfo);
//		
//				// for each url link in page, add it to the queue
//	    		for (URLInfo pageLink : linksInPage) {
//	    			crawler.linkQueue.addLink(pageLink);
//	    			logger.info("Worker "+thread+" "+pageLink.toString() + ": adding to queue");
//	    		}
//	    		
//	    		crawler.notifyThreadExited(thread);
//				
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			
//		}
		//System.exit(0);
		return;
	}
}
