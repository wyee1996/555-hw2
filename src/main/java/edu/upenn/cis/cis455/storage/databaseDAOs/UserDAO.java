package edu.upenn.cis.cis455.storage.databaseDAOs;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;
import com.sleepycat.persist.model.Relationship;
import com.sleepycat.persist.model.SecondaryKey;

import edu.upenn.cis.cis455.storage.DatabaseEnvironment;

import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sleepycat.je.DatabaseException;
import com.sleepycat.persist.model.DeleteAction;

@Entity
public class UserDAO {
	final static Logger logger = LogManager.getLogger(UserDAO.class);

	@PrimaryKey
	private String username;
	
	private int id;
		
	private String password;
	
//	@SecondaryKey(relate=Relationship.ONE_TO_ONE, relatedEntity = SessionDAO.class,
//			onRelatedEntityDelete=DeleteAction.CASCADE)
//	private String sessionId;
	
	@SecondaryKey(relate=Relationship.MANY_TO_MANY, relatedEntity = ChannelDAO.class)
	private Set<String> channels = new HashSet<String>();
	
	public UserDAO() {}
	
	public UserDAO(String username, String password) {
		setUsername(username);
		setPassword(password);
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public void setPassword(String password) {
		try {
			this.password = DatabaseEnvironment.userDataAccessor.encryptPassword(password);
			logger.info("Cleartext Password for user "+this.username+" is "+password);
			logger.info("Encrypted Password for user "+this.username+" is "+this.password);
		} catch (NoSuchAlgorithmException e) {
		}
	}
	
//	public void setSessionId(String sessionId) {
//		this.sessionId = sessionId;
//	}
	
	public int getId() {
		return this.id;
	}
	
	public String getUsername() {
		return this.username;
	}
	
	public String getPassword() {
		return this.password;
	}
	
//	public String getSessionId() {
//		return this.sessionId;
//	}
	
	

}
