package edu.upenn.cis.cis455.xpathengine;

/**
 This class encapsulates the tokens we care about parsing in XML (or HTML)
 */
public class OccurrenceEvent {
	public enum Type {Open, Close, Text};
	
	Type type;
	String value;
	//String docId;
	int depth;
	String encapsulatingElement;
	String url;
	String contentType;
	
	public OccurrenceEvent(Type t, String value, int depth) {
		this.type = t;
		this.value = value;
		//this.docId = docId;
		this.depth = depth;
	}
	
	public void setEncapsulatingElement(String element) {
		this.encapsulatingElement = element;
	}
	
	public String getEncapsulatingElement() {
		return this.encapsulatingElement;
	}
	
	public int getDepth() {
		return this.depth;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getContentType() {
		return this.contentType;
	}
	
	public String getUrl() {
		return this.url;
	}
//	public String getDocId() {
//		return this.docId;
//	}
//	
//	public void setDocId(String docId) {
//		this.docId = docId;
//	}
	

	public String toString() {
		if (type == Type.Open) 
			return "<" + value + ">";
		else if (type == Type.Close)
			return "</" + value + ">";
		else
			return value;
	}
}
