package edu.upenn.cis.cis455.databaseTests;

import static org.junit.Assert.*;

import java.io.IOException;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.upenn.cis.cis455.storage.DatabaseEnvironment;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.storage.dataAccessors.UserDataAccessor;
import edu.upenn.cis.cis455.storage.dataAccessors.WebPageDataAccessor;
import edu.upenn.cis.cis455.storage.databaseDAOs.WebPageDAO;

/**
 * Test variety of cases when getting/adding webpages to database
 * @author vagrant
 *
 */
public class WebPageDatabaseTest {

final static Logger logger = LogManager.getLogger(WebPageDatabaseTest.class);
	
	static String homeDirectory = "/vagrant/testDatabase";
	static StorageInterface database;
	static WebPageDataAccessor webPageDataAccessor;
	
	String testPageContent1 = "Hello, world!";
	String testUrl1 = "/abc/123";
	String testUrl2 = "/def/456";
	
//	@BeforeClass
//	public static void setup() throws IOException {
//		org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
//		logger.info("Starting WebPageDatabaseTest suite");
//			
//		SetupDatabase.purgeDatabase(homeDirectory);	
//		
//		database = StorageFactory.getDatabaseInstance(homeDirectory, false);	
//		
//		webPageDataAccessor = DatabaseEnvironment.webPageDataAccessor;
//	}
//	
//	// Test1: Test that DatabaseEnvironment able to store a webpage to the database
//		// Should not throw an error
//	@Test
//	public void test1() {		
//		webPageDataAccessor.addDocument(testPageContent1, testUrl1,"text/html");
//		logger.info("Test1 passed: Test that DatabaseEnvironment able to store a webpage to the database");
//	}	
//	
//	// Test2: Test that DatabaseEnvironment able to retrieve testPageContent1 using url testUrl1
//		// the webpage's content should equal testPageContent1
//	@Test
//	public void test2() {
//		WebPageDAO webpage = webPageDataAccessor.getWebPageForUrl(testUrl1);
//		assertTrue(webpage.getWebPageContents().toString().equals(testPageContent1));
//		System.out.println(webpage.getWebPageContents());
//		logger.info("Test2 passed: Test that StorageFactory able to retrieve testPageContent1 using url testUrl1");
//	}
//	
//	// Test3: Test that DatabaseEnvironment able to multiple urls for a given webpage
//		// The webpage's content should equal testPageContent1
//	@Test
//	public void test3() {
//		webPageDataAccessor.addDocument(testPageContent1, testUrl2, "text/html");
//		WebPageDAO webpage = webPageDataAccessor.getWebPageForUrl(testUrl2);
//		assertTrue(webpage.getWebPageContents().toString().equals(testPageContent1));
//		logger.info("Test3 passed: Test that DatabaseEnvironment able to multiple urls for a given webpage");
//	}

}
