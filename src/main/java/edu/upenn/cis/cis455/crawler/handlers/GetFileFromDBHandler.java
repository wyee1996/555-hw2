package edu.upenn.cis.cis455.crawler.handlers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.crawler.WebInterface;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.storage.dataAccessors.WebPageDataAccessor;
import edu.upenn.cis.cis455.storage.databaseDAOs.WebPageDAO;
import spark.Request;
import spark.Response;
import spark.Route;
import static spark.Spark.*;

public class GetFileFromDBHandler implements Route {
	final static Logger logger = LogManager.getLogger(GetFileFromDBHandler.class);

	StorageInterface db;
	
	public GetFileFromDBHandler(StorageInterface db) {
		this.db = db;
	}
	
	@Override
	public Object handle(Request request, Response response) throws Exception {
		logger.info("Attempting to retrieve a page from the DB");
//		else {
//		if (response.status() - 300 < 100) {
//			return null;
//		}
		try {
			WebPageDataAccessor webPageDataAccessor = new WebPageDataAccessor(db.getEntityStore());
			String url = request.queryParams("url");
			WebPageDAO webPage = webPageDataAccessor.getWebPageForUrl(url);
			String contentType = webPage.getContentType();
			String doc = db.getDocument(url);
			response.header("Content-Type", contentType);
			return doc;
		} catch (Exception e) {
			halt(404, "File doesn't exist!");
		}
//		}
		return "";
	}

}
