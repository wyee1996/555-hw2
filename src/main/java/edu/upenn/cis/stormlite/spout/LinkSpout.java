package edu.upenn.cis.stormlite.spout;

import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.crawler.Crawler;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Values;

public class LinkSpout implements IRichSpout {
	static final Logger logger = LogManager.getLogger(LinkSpout.class);
	
	String executorId = UUID.randomUUID().toString();
	
	SpoutOutputCollector collector;
	
	public LinkSpout() {
		logger.info("Starting spout");
		System.out.println("Starting spout");
	}
	
	@Override
	public String getExecutorId() {
		return executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("url"));
		
	}

	@Override
	public void open(Map<String, String> config, TopologyContext topo, SpoutOutputCollector collector) {
		this.collector = collector;
		
		try {
			logger.info(getExecutorId()+" starting " +getExecutorId()+" spout");
			System.out.println(getExecutorId()+" starting " +getExecutorId()+" spout");
			
		} catch(Exception e) {
			logger.error("Error starting "+getExecutorId()+" spout");
			System.out.println("Error starting "+getExecutorId()+" spout");
			e.printStackTrace();
		}
	}

	@Override
	public void close() {
		if (Crawler.linkQueue != null) {
			try {
				logger.info("Shutting down "+getExecutorId()+" spout");
				System.out.println("Shutting down "+getExecutorId()+" spout");
			}
			catch(Exception e) {
				logger.error("Error shutting down "+getExecutorId()+" spout");
				System.out.println("Error shutting down "+getExecutorId()+" spout");
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void nextTuple() {
		if (Crawler.linkQueue != null) {
			try {
				URLInfo url = (URLInfo)Crawler.linkQueue.poll();
				if (url != null) {
					logger.info(getExecutorId()+" emitting "+url.toString());
					System.out.println(getExecutorId()+" emitting "+url.toString());
					//collector.emit(new Values<Object>(url));
					collector.emit(new Values<Object>(url));
				}
			}
			catch(Exception e) {
				logger.error("Error getting next tuple for "+getExecutorId());
				System.out.println("Error getting next tuple for "+getExecutorId());
				e.printStackTrace();
			}
		}
		Thread.yield();
	}

	@Override
	public void setRouter(IStreamRouter router) {
		this.collector.setRouter(router);		
	}

}
