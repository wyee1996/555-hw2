package edu.upenn.cis.cis455.xpathengine;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.crawler.handlers.LoginHandler;

public class QueryIndexLists {
	Logger logger = LogManager.getLogger(QueryIndexLists.class);

	ArrayList<PathNode> candList = new ArrayList<PathNode>();
	
	ArrayList<PathNode> waitList = new ArrayList<PathNode>();
	
	public ArrayList<PathNode> getCandList(){
		return this.candList;
	}
	
	public ArrayList<PathNode> getWaitList(){
		return this.waitList;
	}
	
	public boolean candListMatches(String element, int position) {
		for (PathNode candNode : candList) {
			if (candNode.getNodeName().equals(element) && candNode.getPosition() == position) {
				return true;
			}
		}
		return false;
	}
	
	public void addToCandList(PathNode node) {
		if (!candList.contains(node)) {
			candList.add(node);
		}
	}
	
	public void addToWaitList(PathNode node) {
		if (!waitList.contains(node)) {
			waitList.add(node);
		}
	}
	
	public void deleteFromCandList(PathNode node) {
		if (candList.contains(node)) {
			candList.remove(node);
		}
	}
	
	public void deleteFromWaitList(PathNode node) {
		if (waitList.contains(node)) {
			waitList.remove(node);
		}
	}
	
	public void promote(PathNode node) {
		if (waitList.contains(node) && !candList.contains(node)) {
			PathNode newNode = node;
			candList.add(newNode);
		}
	}
	
}
