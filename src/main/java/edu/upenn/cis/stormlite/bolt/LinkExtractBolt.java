package edu.upenn.cis.stormlite.bolt;

import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.crawler.Crawler;
import edu.upenn.cis.cis455.crawler.PageParser;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.xpathengine.DocumentInfo;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;

public class LinkExtractBolt implements IRichBolt{

	String executorId = UUID.randomUUID().toString();
	
	static final Logger logger = LogManager.getLogger(LinkExtractBolt.class);
	
	Fields myFields = new Fields();
	
	public LinkExtractBolt() {}
	
	@Override
	public String getExecutorId() {
		return executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(myFields);		
	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void execute(Tuple input) {
		DocumentInfo docInfo = (DocumentInfo)input.getObjectByField("documentInfo");
		String documentBody = docInfo.getDocumentBody();
		String hostname = (String)input.getObjectByField("hostname");
		
		System.out.println(getExecutorId() + ": starting execution on doc "+documentBody.substring(0,10)+" ...");
		
		try {
			
			ArrayList<URLInfo> linksInPage = PageParser.parsePage(hostname, documentBody);
			
			for (URLInfo link : linksInPage) {
				if (!Crawler.reachedCountLimit()) {
					System.out.println(getExecutorId()+ ": adding link "+link.toString()+" to queue");
					Crawler.linkQueue.offer(link);
				}
			}
			
		}catch(Exception e) {
			logger.error(getExecutorId() + ": error extracting document links for doc "+documentBody.substring(0,10)+" ...");
			e.printStackTrace();
		}
		System.out.println(getExecutorId() + ": " + input.toString());
		
	}

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		//this.collector = collector;		
	}

	@Override
	public void setRouter(IStreamRouter router) {
		//this.collector.setRouter(router);
		
	}

	@Override
	public Fields getSchema() {
		return myFields;
	}

}
