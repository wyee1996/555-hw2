package edu.upenn.cis.cis455.xpathengine;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Set;

public class PathNode {

	private String elementString = "";
	
	private String queryId;
	
	private String nodeName;
	
	private int position;
	
	private ArrayList<String> prefixVals;
	
	private Hashtable<String, String> bracketText = new Hashtable<String, String>();
	
	private PathNode root;
	
	private PathNode parent;
	
	private PathNode child;
	
	public PathNode(String queryId, int position) {
		this.queryId = queryId;
		this.position = position;
	}
	
	public String getNodeName() {
		return this.nodeName;
	}
	
	public String getElementString() {
		return this.elementString;
	}
	
	public String getQueryId() {
		return queryId;
	}
	
	public int getPosition() {
		return position;
	}
	
	public ArrayList<String> getPrefixVals(){
		return prefixVals;
	}
	
	public PathNode getChild() {
		return this.child;
	}
	
	public PathNode getParent() {
		return this.parent;
	}
	
	public PathNode getRoot() {
		return this.root;
	}
	
	public Hashtable<String, String> getBracketText(){
		return this.bracketText;
	}
	
	public void setElementString(String elementString) {
		this.elementString = elementString;
	}
	
	public void addToElementString(Character ch) {
		this.elementString += ch;
	}
	
	public void setQueryId(String queryId) {
		this.queryId = queryId;
	}
	
	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}
	
	public void setPosition(int pos) {
		this.position = pos;
	}
	
	public void setChild(PathNode child) {
		this.child = child;
	}
	
	public void setParent(PathNode parent) {
		this.parent = parent;
	}
	
	public void setRoot(PathNode root) {
		this.root = root;
	}
	
	public void addToBracketText(String bracketExp, String textOrContains) {
		this.bracketText.put(bracketExp, textOrContains);
	}
	
	public void print() {
		StringBuilder sb = new StringBuilder();
		sb.append("Element String: "+elementString);
		sb.append(" Query Id: "+queryId);
		sb.append(" Node Name: "+nodeName);
		sb.append(" Position: "+position);
		Set<String> keys = bracketText.keySet();
		for (String key : keys) {
			sb.append(" Bracket Text: "+key);
		}
		if (parent != null) {
			sb.append(" Parent: "+parent.getNodeName());
		}
		if (child != null) {
			sb.append(" Child: "+child.getNodeName());
		}
		System.out.println(sb.toString());
	}
	
}
