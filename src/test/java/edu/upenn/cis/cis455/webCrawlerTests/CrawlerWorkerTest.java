package edu.upenn.cis.cis455.webCrawlerTests;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import org.junit.Test;

public class CrawlerWorkerTest {
	
	String CRLF = "\r\n";
	
	@Test
	public void test() throws UnknownHostException, IOException {
		Socket socket = new Socket("crawltest.cis.upenn.edu",80);
		System.out.println(socket.isConnected());
		
		String request = "GET /robots.txt HTTP/1.1"+CRLF
						+"Host: crawltest.cis.upenn.edu"+CRLF
						+"User-Agent: cis455crawler"+CRLF +CRLF;
		
		OutputStream output = socket.getOutputStream();
        PrintWriter writer = new PrintWriter(output, true);

		writer.println(request);
        		
		InputStream input = socket.getInputStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(input));
		String line;
		 
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }
		socket.close();
	}

}
