package edu.upenn.cis.cis455.storage;

import java.io.File;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import static spark.Spark.*;

import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.StoreConfig;

import edu.upenn.cis.cis455.storage.dataAccessors.ChannelDataAccessor;
import edu.upenn.cis.cis455.storage.dataAccessors.DataAccessor;
import edu.upenn.cis.cis455.storage.dataAccessors.SessionDataAccessor;
import edu.upenn.cis.cis455.storage.dataAccessors.UserDataAccessor;
import edu.upenn.cis.cis455.storage.dataAccessors.WebPageDataAccessor;
import edu.upenn.cis.cis455.storage.databaseDAOs.SessionDAO;
import edu.upenn.cis.cis455.storage.databaseDAOs.UserDAO;
import edu.upenn.cis.cis455.storage.databaseDAOs.WebPageDAO;

public final class DatabaseEnvironment implements StorageInterface{

	public static Environment env;
	public static EntityStore store;
	public static SessionDataAccessor sessionDataAccessor;
	public static UserDataAccessor userDataAccessor;
	public static WebPageDataAccessor webPageDataAccessor;
	public static ChannelDataAccessor channelDataAccessor;
	
	public void setup(File homeDirectory, boolean readOnly) throws DatabaseException{
		
		EnvironmentConfig envConfig = new EnvironmentConfig();
		StoreConfig storeConfig = new StoreConfig();
		
		envConfig.setReadOnly(readOnly);
		storeConfig.setReadOnly(readOnly);
		
		// If environment opened for write, then we want to be able to
		// create the environment and entity store if they don't exist
		envConfig.setAllowCreate(true);
		storeConfig.setAllowCreate(true);
		
		envConfig.setTransactional(true);
		storeConfig.setTransactional(true);
		
		// open the environment and entity store
		env = new Environment(homeDirectory, envConfig);
		store = new EntityStore(env, "EntityStore", storeConfig);
		
		// open the Data Accessors
		sessionDataAccessor = new SessionDataAccessor(store);
		userDataAccessor = new UserDataAccessor(store);
		webPageDataAccessor = new WebPageDataAccessor(store);
		channelDataAccessor = new ChannelDataAccessor(store);
		
	}
	
	// Return a handle to the entire store
	@Override
	public EntityStore getEntityStore() {
		return store;
	}
	
	// Return a handle to the environment
	@Override
	public Environment getEnvironment() {
		return env;
	}
	
	// Close store and environment
	public void close() {
		if (store != null) {
			try {
				store.close();
			}
			catch(DatabaseException dbe) {
				System.err.println("Error closing store: "+dbe.toString());
				System.exit(-1);
			}
		}
		if (env != null) {
			try {
				env.close();
			}
			catch(DatabaseException dbe){
				System.err.println("Error closing environment: "+dbe.toString());
				System.exit(-1);
			}
		}
	}

	@Override
	public int getCorpusSize() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int addDocument(String url, String documentContents, String contentType) {
		webPageDataAccessor.addDocument(documentContents, url, contentType);
		return 0;
	}

	@Override
	public String getDocument(String url) {
		return webPageDataAccessor.getWebPageForUrl(url).getWebPageContents();
	}

	@Override
	public int addUser(String username, String password) {
		UserDAO newUser = new UserDAO(username, password);
		userDataAccessor.updateUser(newUser);		
		return 0;
	}

	@Override
	public boolean loginUser(String username, String password) {
		
		try {
			UserDAO user;
			user = userDataAccessor.getUserForUsernameCheckPass(username, password);
			if (user != null) {
				return true;
			}
		} catch (DatabaseException | NoSuchAlgorithmException e) {
			return false;
		}		
		return false;
	}

	/**
	 * Checks whether the user is logged in or not (has an unexpired Session)
	 * @param username
	 * @return
	 */
	@Override
	public boolean isUserLoggedIn(String username) {
		UserDAO user = userDataAccessor.getUserForUsername(username);
		if (user != null) {
//			String sessionId = user.getSessionId();
//			SessionDAO session = sessionDataAccessor.getSessionBySessionId(sessionId);
//			if (session != null) {
//				
//				// If session's expiration date after current time, then 
//				// it hasn't expired yet
//				if (session.getExpiresTime().after(new Date())) {
//					
//					return true;					
//				}
//				
//				// Otherwise, this means that Session has expired so delete it
//				else {
//					sessionDataAccessor.deleteSession(session.getSessionId());
//					return false;
//				}
//				
//			}
//			else {				
//				// The session has been deleted, so user is logged out
//				return false;
//			}
		}
		
		// If the user does not exist, then this should return an exception that user doesn't exist
		halt(401);
		return false;
	}
}
