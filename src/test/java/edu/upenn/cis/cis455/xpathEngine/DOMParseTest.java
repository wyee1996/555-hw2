package edu.upenn.cis.cis455.xpathEngine;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.logging.log4j.Level;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;
import org.jsoup.select.NodeTraversor;
import org.jsoup.select.NodeVisitor;
import org.junit.Test;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import edu.upenn.cis.cis455.crawler.Crawler;
import edu.upenn.cis.cis455.databaseTests.SetupDatabase;
import edu.upenn.cis.cis455.xpathengine.OccurrenceEvent;
import edu.upenn.cis.cis455.xpathengine.SAXParseHandler;

public class DOMParseTest {

//	@Test
//	public void test() throws IOException {
//		SetupDatabase.purgeDatabase("/vagrant/database");
//		org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
//		String[] args = {"https://crawltest.cis.upenn.edu/misc/weather.xml", "/vagrant/database", "5", "5"};
//		Crawler.main(args);
//	}
	
	@Test
	public void test1() {
		String html = "<!DOCTYPE html>"
					+ "<html>"
						+ "<head>"
							+ "<title>Test</title>"
						+ "</head>"
						+ "<body>"
							+ "<p>Hello world</p>"
						+ "</body>"
					+ "</html>";
		Document doc = Jsoup.parse(html);
		Elements all = doc.getAllElements();
		traverse(all.first());
	}
	
	@Test
	public void test2() {
		String html = "<!DOCTYPE html>"
					+ "<html>"
						+ "<head>"
						+ "</head>"
						
						+ "<body>"
						+ "<h2 title=\"I'm a header\">The title Attribute</h2>"
							+ "<p>Hello world</p>"
						+ "</body>"
					+ "</html>";
		Document doc = Jsoup.parse(html);
		Elements all = doc.getAllElements();
		traverse(all.first());
	}
	
	public void traverse(Element root) {
        Element node = root;
        int depth = 0;
        boolean outputtedText = false;
        int numChildNodes = node.childNodeSize();
        while (node != null) {
        	if (!outputtedText) {
        		head(node);
        	}
        	else outputtedText = false;
        	
            if (numChildNodes > 0) {
            	try {
            		node = node.child(0);
            		numChildNodes = node.childNodeSize();
            		depth++;
            	}
            	catch (Exception e){
            		numChildNodes = 0;
            		Node newNode = node.childNode(0);
            		text(newNode);
            		outputtedText = true;
            	}
                
            } else {
                while (node.nextSibling() == null && depth > 0) {
                    tail(node);
                    node = node.parent();
                    numChildNodes = node.childNodeSize();
                    depth--;
                }
                tail(node);
                if (node == root)
                    break;
                node = node.nextElementSibling();
                numChildNodes = node.childNodeSize();
            }
        }
    }
	
	public void head(Element node) {
		System.out.println("open name: "+node.nodeName());
	}
	
	private void tail(Element node) {
		System.out.println("close name: "+node.nodeName());
	}
	
	private void text(Node node) {
		System.out.println("text: "+node.toString());
	}

}
