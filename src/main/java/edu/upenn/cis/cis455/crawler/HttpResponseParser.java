package edu.upenn.cis.cis455.crawler;

import static spark.Spark.*;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class HttpResponseParser {

	
	static final int BUFSIZE = 8192;
	final static Logger logger = LogManager.getLogger(HttpResponseParser.class);
    
    /**
     * Decodes the sent headers and loads the data into Key/value pairs
     */
    public static boolean decodeHeader(BufferedReader in, Map<String, String> pre,
                                    Map<String, String> headers){
        try {
            // Read the request line
            String requestLine = in.readLine();
            if (requestLine == null) {
                return false;
            }
            StringTokenizer st = new StringTokenizer(requestLine);

            // Get protocol version
            if (!st.hasMoreTokens()) {
                return false;
            }
            pre.put("protocolVersion", st.nextToken());

            // Get response status
            if (!st.hasMoreTokens()) {
            	return false;
            }
            pre.put("statusCode", st.nextToken());
            
            // Get response code
            if (!st.hasMoreTokens()) {
            	return false;
            }
            pre.put("statusReason", st.nextToken());

            // Get Headers
            String line = in.readLine();
            while (line != null && !line.trim().isEmpty()) {
                int p = line.indexOf(':');
                if (p >= 0) {
                    headers.put(line.substring(0, p).trim().toLowerCase(Locale.US), line.substring(p + 1).trim());
                }
                line = in.readLine();
            }
            return true;
        } catch (IOException ioe) {
        	return false;
        }
    }
    
    /**
     * Decode percent encoded <code>String</code> values.
     * 
     * @param str
     *            the percent encoded <code>String</code>
     * @return expanded form of the input, for example "foo%20bar" becomes
     *         "foo bar"
     */
    public static String decodePercent(String str) {
        String decoded = null;
        try {
            decoded = URLDecoder.decode(str, "UTF8");
        } catch (UnsupportedEncodingException ignored) {
            logger.warn("Encoding not supported, ignored", ignored);
        }
        return decoded;
    }
    
    /**
     * Parse the initial response header
     * 
     * @param remoteIp IP address of client
     * @param inputStream Socket input stream (not yet read)
     * @param headers Map to receive header key/values
     */
    public static BufferedReader parseResponse( 
                        InputStream inputStream, 
                        Map<String, String> headers) throws IOException{
        int splitbyte = 0;
        int rlen = 0;     
        
        // Read the first 8192 bytes.
        // The full header should fit in here.
        // Apache's default header limit is 8KB.
        // Do NOT assume that a single read will get the entire header
        // at once!
        byte[] buf = new byte[BUFSIZE];
        splitbyte = 0;
        rlen = 0;

        int read = -1;
        inputStream.mark(BUFSIZE);
        try {
        	// returns total # of bytes read into buf or -1 if end of stream has been reached
            read = inputStream.read(buf, 0, BUFSIZE);
        } catch (IOException e) {
            return null;
        }
        if (read == -1) {
            return null;
        }
        while (read > 0) {
            rlen += read;
            splitbyte = findHeaderEnd(buf, rlen);
            if (splitbyte > 0) {
                break;
            }
            read = inputStream.read(buf, rlen, BUFSIZE - rlen);
        }

        if (splitbyte < rlen) {
            inputStream.reset();
        }

        headers.clear();

        // Create a BufferedReader for parsing the header.
        BufferedReader hin = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(buf, 0, rlen)));

        // Decode the header into parms and header java properties
        Map<String, String> pre = new HashMap<String, String>();
        if (!decodeHeader(hin, pre, headers)) {
        	return null;
        }

        BufferedReader bufferedBody = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(buf, splitbyte, rlen)));

        headers.put("protocolVersion", pre.get("protocolVersion"));
        headers.put("statusCode", pre.get("statusCode"));
        
        return bufferedBody;
    }
    
    /**
     * Find byte index separating header from body. It must be the last byte of
     * the first two sequential new lines.
     */
    static int findHeaderEnd(final byte[] buf, int rlen) {
        int splitbyte = 0;
        while (splitbyte + 1 < rlen) {

            // RFC2616
            if (buf[splitbyte] == '\r' && buf[splitbyte + 1] == '\n' && splitbyte + 3 < rlen && buf[splitbyte + 2] == '\r' && buf[splitbyte + 3] == '\n') {
                return splitbyte + 4;
            }

            // tolerance
            if (buf[splitbyte] == '\n' && buf[splitbyte + 1] == '\n') {
                return splitbyte + 2;
            }
            splitbyte++;
        }
        return 0;
    }
}
