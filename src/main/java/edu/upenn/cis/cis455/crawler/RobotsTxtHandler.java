package edu.upenn.cis.cis455.crawler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.crawler.utils.RobotsTxtInfo;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;

/**
 * Parses robots.txt file and saves data to RobotsTxtInfo
 * @author vagrant
 *
 */
public class RobotsTxtHandler {	
	static final Logger logger = LogManager.getLogger(RobotsTxtHandler.class);

	
	public static String getApplicableUserAgent(RobotsTxtInfo robotsInfo) {
		
		// Check if robots.txt has an userAgent entry for cis455crawler
		if (robotsInfo.containsUserAgent("cis455crawler")) {
			return "cis455crawler";
		}
		
		// If not, then check if has entry for *
		if (robotsInfo.containsUserAgent("*")) {
			return "*";
		}
		
		// Otherwise, return "all" for no restrictions
		return "all";
	}
	
	public static void extendCrawlDelayTime(RobotsTxtInfo robotsInfo) {
		
		String userAgent = getApplicableUserAgent(robotsInfo);
		
		Integer crawlDelay = robotsInfo.getCrawlDelay(userAgent);
		
		if (crawlDelay != null) {
			Date nextCrawlTime = DateUtils.addSeconds(new Date(), crawlDelay);
			robotsInfo.getCrawlDelayTimes().replace(userAgent, nextCrawlTime);
		}
	}
	
	/**
	 * Checks if a given link is a disallowed link
	 * Returns true if it is disallowed
	 * @param disallowedLinks
	 * @param link
	 * @return
	 */
	public static boolean checkIfDisallowed (ArrayList<String> disallowedLinks, ArrayList<String> allowedLinks, URLInfo link) {
		
		String linkString = link.getFilePath();
		
		if (allowedLinks != null) {
			for (String allowedLink : allowedLinks) {
				if (allowedLink.equals(linkString)) return false;
			}
		}		
		
		// check if url equals one of disallowed links or if it's a child of a disallowed link
		// if it's a child of a disallowed link but is an allowed link, then we are okay
		if (disallowedLinks != null) {
			for (String disallowedLink : disallowedLinks) {
				if (disallowedLink.equals("/")) {
					logger.info(disallowedLink+": can't go here!");
					return true;
				}
				if (disallowedLink.startsWith(linkString) && !linkString.equals("/")) {		
					logger.info(disallowedLink+": can't go here!");
					return true;
				}
			}
		}
		
		return false;
	}
	
	public static BufferedReader getRobotsTxt(String link) throws IOException {
		URL url = new URL(link);
		HttpURLConnection con = (HttpURLConnection)url.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("User-Agent", "cis455crawler");
		if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			return in;
		}
		return null;
	}
	
	public static RobotsTxtInfo parse(BufferedReader reader) throws IOException {
		
		RobotsTxtInfo robotsTxtInfo = new RobotsTxtInfo();
		
		String line;
		String useragent = "";
		while ((line = reader.readLine()) != null) {
			line = line.trim();
			
			if (!line.isEmpty() && !line.startsWith("#")) {
				line = line.replaceAll("<[^>]+>", "");
				
				// get rid of in-line comments
				int commentIdx = line.indexOf("#");
				if (line.indexOf("#") > -1) {
					line = line.substring(0, commentIdx);
					line = line.trim();
				}
				
				// grab the various robots paths
				if (line.matches("(?i)^User-agent:.*")){
					useragent = line.substring(11).trim().toLowerCase();
					robotsTxtInfo.addUserAgent(useragent);
				}
				else if (line.matches("(?i)Disallow:.*")) {
					if (useragent == "") continue;
					String disallowedPath = line.substring(9).trim();
					robotsTxtInfo.addDisallowedLink(useragent, disallowedPath);
					robotsTxtInfo.addSitemapLink(disallowedPath);
				}
				else if (line.matches("(?i)Crawl-delay:.*")){
					if (useragent == "") continue;
					String crawlDelay = line.substring(12).trim();
					try {
						crawlDelay = crawlDelay.split("[^\\d\\.]+")[0];
						robotsTxtInfo.addCrawlDelay(useragent, Integer.parseInt(crawlDelay));
					}
					catch (Exception ex){}
				}
				else if (line.matches("(?i)Allow:.*")){
					if (useragent == "") continue;
					String allowedPath = line.substring(6).trim();
					robotsTxtInfo.addAllowedLink(useragent, allowedPath);
					robotsTxtInfo.addSitemapLink(allowedPath);
				}
			}		
		}
		return robotsTxtInfo;
	}
}

 