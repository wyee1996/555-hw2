package edu.upenn.cis.cis455.crawler;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.cis455.crawler.utils.RobotsTxtInfo;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.storage.dataAccessors.ChannelDataAccessor;
import edu.upenn.cis.cis455.storage.databaseDAOs.ChannelDAO;
import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.LocalCluster;
import edu.upenn.cis.stormlite.Topology;
import edu.upenn.cis.stormlite.TopologyBuilder;
import edu.upenn.cis.stormlite.bolt.DOMParseBolt;
import edu.upenn.cis.stormlite.bolt.DocFetcherBolt;
import edu.upenn.cis.stormlite.bolt.LinkExtractBolt;
import edu.upenn.cis.stormlite.bolt.PathMatcherBolt;
import edu.upenn.cis.stormlite.spout.LinkSpout;
import edu.upenn.cis.stormlite.spout.TestSpout;
import edu.upenn.cis.stormlite.tuple.Fields;


public class Crawler{
	static final Logger logger = LogManager.getLogger(Crawler.class);
    //static final int NUM_WORKERS = 10;
    //static ArrayList<Thread> threadPool;
    //static Hashtable<Integer, Boolean> threadStatus = new Hashtable<Integer, Boolean>();
    public final static Hashtable<String, RobotsTxtInfo> robotsForHostname = new Hashtable<String, RobotsTxtInfo>();
    public final static BlockingQueue<URLInfo> linkQueue = new ArrayBlockingQueue<URLInfo>(200);

	private static final String LINK_SPOUT = "LINK_SPOUT";
    private static final String DOC_FETCHER_BOLT = "DOC_FETCHER_BOLT";
    private static final String LINK_EXTRACT_BOLT = "LINK_EXTRACT_BOLT";
    private static final String DOM_PARSE_BOLT = "DOM_PARSE_BOLT";
    private static final String PATH_MATCHER_BOLT = "PATH_MATCHER_BOLT";
    
    URLInfo startUrl;
    public static StorageInterface db;
    private static Integer numActiveUrls;
    int size;
    static int maxNumLinksVisited = 0;
    static int numLinksVisited;
    private static ArrayList<String> expressions = new ArrayList<String>();

    public Crawler(String startUrl, StorageInterface db, int size, int count) {
    	this.startUrl = new URLInfo(startUrl);
    	this.db = db;
    	this.size = size;
    	maxNumLinksVisited = count;
    	
    }
    
    /**
     * Main thread
     * @throws InterruptedException 
     */
    public void start() throws InterruptedException {
    	
    	// add the seed to the queue
    	linkQueue.offer(startUrl);
    	
    	// get expressions
    	ChannelDataAccessor channelDataAccess = new ChannelDataAccessor(Crawler.db.getEntityStore());
		ArrayList<ChannelDAO> channels = channelDataAccess.getAllChannels();
		for (ChannelDAO channel : channels) {
			expressions.add(channel.getXPath());
		}
    	    	
    	Config config = new Config();

        LinkSpout linkSpout = new LinkSpout();
        DocFetcherBolt docFetcherBolt = new DocFetcherBolt();
        LinkExtractBolt linkExtractBolt = new LinkExtractBolt();
        DOMParseBolt domParseBolt = new DOMParseBolt();
        PathMatcherBolt pathMatchBolt = new PathMatcherBolt();
        //TestSpout testSpout = new TestSpout();

        TopologyBuilder builder = new TopologyBuilder();
        
        builder.setSpout(LINK_SPOUT, linkSpout, 2);
        builder.setBolt(DOC_FETCHER_BOLT, docFetcherBolt, 2).shuffleGrouping(LINK_SPOUT);
        builder.setBolt(LINK_EXTRACT_BOLT, linkExtractBolt, 2).shuffleGrouping(DOC_FETCHER_BOLT);  
        builder.setBolt(DOM_PARSE_BOLT, domParseBolt, 2).shuffleGrouping(DOC_FETCHER_BOLT);
//        builder.setSpout("TEST_SPOUT", testSpout, 1);
//        builder.setBolt(DOM_PARSE_BOLT, domParseBolt, 1).shuffleGrouping("TEST_SPOUT");
        builder.setBolt(PATH_MATCHER_BOLT, pathMatchBolt, 2).fieldsGrouping(DOM_PARSE_BOLT, new Fields("documentBody"));
        
        
        LocalCluster cluster = new LocalCluster();
        Topology topo = builder.createTopology();

        ObjectMapper mapper = new ObjectMapper();
        try {
			String str = mapper.writeValueAsString(topo);

			System.out.println("The StormLite topology is:\n" + str);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        cluster.submitTopology("test", config, 
        		builder.createTopology());
        while (!isDone())
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        cluster.killTopology("test");
        cluster.shutdown();
        System.exit(0);
    }
    
    /**
     * Returns true if it's permissible to access the site right now
     * eg due to robots, etc.
     */
    public static boolean isOKtoCrawl(URLInfo site) { 
    	
    	RobotsTxtInfo robotsInfo = robotsForHostname.get(site.getHostName());
    	if (robotsInfo == null) return false;
    	
    	String applicableUserAgent = RobotsTxtHandler.getApplicableUserAgent(robotsInfo);
    	
    	if (applicableUserAgent.equals("all")) return false;
    	
    	return RobotsTxtHandler.checkIfDisallowed(robotsInfo.getDisallowedLinks(applicableUserAgent), 
    					robotsInfo.getAllowedLinks(applicableUserAgent), site);
	}

    /**
     * Returns true if the crawl delay says we should wait
     */
    public static Integer deferCrawl(URLInfo site) { 
    	
    	RobotsTxtInfo robotsInfo = robotsForHostname.get(site.getHostName());
    	if (robotsInfo == null) return null;
    	
    	String applicableUserAgent = RobotsTxtHandler.getApplicableUserAgent(robotsInfo);
    	
    	if (applicableUserAgent.equals("all")) return null;
    	
    	Integer crawlDelay = robotsInfo.getCrawlDelay(applicableUserAgent);
    	
    	return crawlDelay;
	}
    
    /**
     * Returns true if it's permissible to fetch the content,
     * eg that it satisfies the path restrictions from robots.txt
     */
    public boolean isOKtoParse(URLInfo url) { 
    	
    	return true; 
	}
    
    public static ArrayList<String> getXPathExpressions(){
    	return expressions;
    }
    
    /**
     * Returns true if the document content looks worthy of indexing,
     * eg that it doesn't have a known signature
     */
    public boolean isIndexable(String content) { return true; }
    
    /**
     * We've indexed another document
     */
    public static void incCount() { 
    	numLinksVisited += 1;
    	System.out.println("Number of links visited: "+numLinksVisited);
    }
    
    public static boolean reachedCountLimit() {
    	if (numLinksVisited >= maxNumLinksVisited) return true;
    	return false;
    }
    
    /**
     * Workers can poll this to see if they should exit, ie the
     * crawl is done
     */
    public static boolean isDone() { 
    	
    	if (numActiveUrls != null) {
    		if (reachedCountLimit() && numActiveUrls == 0) {
    			System.out.println("Finished crawling due to reached count limit of "+maxNumLinksVisited);
    			return true;
    		}
    		if( linkQueue.isEmpty() && numActiveUrls == 0) {
    			System.out.println("Finished crawling because no links left in queue");
    			return true;
    		}
    	}
    	
    	return false;
	}
    
    /**
     * Workers should notify when they are processing an URL
     */
    public static void setWorking(boolean working) {
    	if (working) {
    		if (numActiveUrls == null) numActiveUrls = 0;
    		numActiveUrls++;
    	}
    }
    
    /**
     * Workers should call this when they exit, so the master
     * knows when it can shut down
     */
    public static void notifyThreadExited() {
    	numActiveUrls--;
    }
    
    /**
     * Main program:  init database, start crawler, wait
     * for it to notify that it is done, then close.
     */
    public static void main(String args[]) {
    	org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.INFO);
        if (args.length < 3 || args.length > 5) {
            System.out.println("Usage: Crawler {start URL} {database environment path} {max doc size in MB} {number of files to index}");
            System.exit(1);
        }

        System.out.println("Crawler starting");
        String startUrl = args[0];
        String envPath = args[1];
        Integer size = Integer.valueOf(args[2]);
        Integer count = args.length == 4 ? Integer.valueOf(args[3]) : 100;

        StorageInterface db = StorageFactory.getDatabaseInstance(envPath, false);

        Crawler crawler = new Crawler(startUrl, db, size, count);

        System.out.println("Starting crawl of " + count + " documents, starting at " + startUrl);
        try {
			crawler.start();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

        while (!crawler.isDone())
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        
//        WebPageDataAccessor webPageDataAccess = new WebPageDataAccessor(db.getEntityStore());
//        webPageDataAccess.printAllWebPages();

        // TODO: final shutdown
//        for (Thread thread : threadPool) {
//        	thread.stop();
//        }
        
        //db.close();

        System.out.println("Done crawling!");
    }

}
