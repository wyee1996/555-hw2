package edu.upenn.cis.cis455.databaseTests;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.util.FileUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.upenn.cis.cis455.storage.DatabaseEnvironment;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.storage.dataAccessors.SessionDataAccessor;
import edu.upenn.cis.cis455.storage.dataAccessors.UserDataAccessor;
import edu.upenn.cis.cis455.storage.databaseDAOs.SessionDAO;
import edu.upenn.cis.cis455.storage.databaseDAOs.UserDAO;

/**
 * Tests variety of cases when creating and modifying a database
 * @author vagrant
 *
 */
public class CreateDatabaseTest {
	
		
//		final static Logger logger = LogManager.getLogger(CreateDatabaseTest.class);
//		
//		static String homeDirectory = "/vagrant/testDatabase2";
//		static StorageInterface database;
//		static SessionDataAccessor sessionDataAccessor;
//		static UserDataAccessor userDataAccessor;
//		
//		@BeforeClass
//		public static void setup() throws IOException {
//			org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
//			logger.info("Starting CreateDatabaseTest suite");
//				
//			SetupDatabase.purgeDatabase(homeDirectory);	
//		}
//	
//		// Test1: Test that StorageFactory able to create the database at given directory if doesn't exist
//		@Test
//		public void test1() {
//			// Create a new instance of test database
//			database = StorageFactory.getDatabaseInstance(homeDirectory, false);
//			
//			// Get data accessors
//			this.sessionDataAccessor = DatabaseEnvironment.sessionDataAccessor;
//			this.userDataAccessor = DatabaseEnvironment.userDataAccessor;
//			logger.info("Passed Test1: Test that StorageFactory able to create the database at given directory if doesn't exist");
//		}		
//		
//		// Test2: Test that StorageFactory creates a database and directory if directory exists
//		@Test
//		public void test2() {
//			StorageInterface database = StorageFactory.getDatabaseInstance(homeDirectory, false);
//			//assertTrue(this.database == database);
//			logger.info("Passed Test2: Test that StorageFactory creates a database and directory if directory exists");
//		}
//		
//		// Test3: Test that DatabaseEnvironment can add user to database
//		@Test
//		public void test3() {
//			database.addUser("TestUser", "TestPass");
//			logger.info("Passed Test3: Test that DatabaseEnvironment can add TestUser to database");
//		}		
//		
//		// Test4: Test that DatabaseEnvironment can retrieve user from database
//		@Test
//		public void test4() {
//			UserDAO user = userDataAccessor.getUserForUsername("TestUser");
//			assertTrue(user.getUsername().equals("TestUser"));
//			logger.info("Passed Test4: Test that DatabaseEnvironment can retrieve user"+PrintUserInfo(user)+"from database");
//		}
		
		// Test5: Test that DatabaseEnvironment able to update user session Id
//		@Test
//		public void test5() {
//			UserDAO user = userDataAccessor.getUserForUsername("TestUser");
//			SessionDAO newSession = sessionDataAccessor.createNewSession();
//			user.setSessionId(newSession.getSessionId());
//			userDataAccessor.updateUser(user);
//			
//			// Retrieve same user's session id to see if it has been updated
//			UserDAO retrievedUser = userDataAccessor.getUserForUsername("TestUser");
//			assertTrue(retrievedUser.getSessionId().equals(newSession.getSessionId()));
//			logger.info("Passed Test5: Test that DatabaseEnvironment able to update user session Id w/ info"+PrintUserInfo(retrievedUser));
//		}
		
		// Test6: Test that DatabaseEnvironment is able to delete a user's session Id
//		@Test
//		public void test6() {
//			UserDAO user = userDataAccessor.getUserForUsername("TestUser");
//			String oldSessionId = user.getSessionId();
//			userDataAccessor.deleteUserSession(user);
//			
//			// Check to see if the session Id exists in the Session store
//			SessionDAO retrievedSession = sessionDataAccessor.getSessionBySessionId(oldSessionId);
//			assertTrue(retrievedSession == null);	
//			logger.info("Passed Test6: Test that DatabaseEnvironment is able to delete a user's session Id"+PrintUserInfo(user));
//		}
		
//		public String PrintUserInfo(UserDAO user) {
//			StringBuilder sb = new StringBuilder();
//			sb.append(" Username: "+user.getUsername());
//			sb.append(" Password: "+user.getPassword());
//			return sb.toString();
//		}
		
}
