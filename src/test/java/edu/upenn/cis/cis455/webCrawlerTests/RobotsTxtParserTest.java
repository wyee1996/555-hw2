package edu.upenn.cis.cis455.webCrawlerTests;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.upenn.cis.cis455.crawler.RobotsTxtHandler;

public class RobotsTxtParserTest {

	String robotstxt = "# These defaults shouldn't apply to your crawler\n" + 
			"User-agent: *\n" + 
			"Disallow: /marie/\n" + 
			"Crawl-delay: 10\n" + 
			"\n" + 
			"# Below is the directive your crawler should use:\n" + 
			"User-agent: cis455crawler\n" + 
			"Disallow: /marie/private/\n" + 
			"Disallow: /foo/\n" + 
			"Crawl-delay: 5\n" + 
			"\n" + 
			"# This should be ignored by your crawler\n" + 
			"User-agent: evilcrawler\n" + 
			"Disallow: /";
	
	@Test
	public void test() {
		//RobotsTxtParser.parse(reader)
	}

}
