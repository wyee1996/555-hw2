package edu.upenn.cis.cis455.storage.databaseDAOs;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;
import com.sleepycat.persist.model.Relationship;
import com.sleepycat.persist.model.SecondaryKey;

@Entity
public class WebPageDAO {

	@PrimaryKey
	private int webpage;
	
	@SecondaryKey(relate = Relationship.ONE_TO_MANY)
	private Set<String> urls = new HashSet<String>();
	
	private String pageContents;
	
	private Date timeLastChecked;
	
	private String contentType;
	
	public WebPageDAO() {};
	
	public WebPageDAO(String pageContents, String url, String contentType) {
		this.pageContents = pageContents;
		this.webpage = pageContents.hashCode();
		this.urls.add(url);
		this.contentType = contentType;
	}
	
	public int getWebPageHash() {
		return this.webpage;
	}
	
	public String getContentType() {
		return this.contentType;
	}
	
	public String getWebPageContents() {
		return this.pageContents;
	}
	
	public Set<String> getLinks(){
		return this.urls;
	}
	
	public Date getTimeLastChecked() {
		return this.timeLastChecked;
	}
	
	public void setTimeLastChecked(Date timeLastChecked) {
		this.timeLastChecked = timeLastChecked;
	}
	
	public void addLinkToWebPage(String url) {
		this.urls.add(url);
		setTimeLastChecked(new Date());
	}
}
