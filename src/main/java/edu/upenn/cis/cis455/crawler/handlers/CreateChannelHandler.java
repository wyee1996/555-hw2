package edu.upenn.cis.cis455.crawler.handlers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.storage.dataAccessors.ChannelDataAccessor;
import edu.upenn.cis.cis455.xpathengine.InvalidXPathException;
import edu.upenn.cis.cis455.xpathengine.PathNode;
import edu.upenn.cis.cis455.xpathengine.XPathParser;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Session;

import static spark.Spark.*;

public class CreateChannelHandler implements Route{
	final static Logger logger = LogManager.getLogger(CreateChannelHandler.class);

	StorageInterface db;
	
	public CreateChannelHandler(StorageInterface db) {
		this.db = db;
	}
	
	@Override
	public Object handle(Request request, Response response) {
		String name = request.params("name");
		String xpath = request.queryParams("xpath");
		
		ChannelDataAccessor channelDataAccess = new ChannelDataAccessor(db.getEntityStore());
		
		if (channelDataAccess.getChannel(name) != null) {
			halt(409, "That channel already exists!");
		}
		
		Session session = request.session(false);
		String user = session.attribute("user");
		
		XPathParser xpathParser = new XPathParser(0);
		try{
			PathNode node = xpathParser.parse(xpath);
		}catch (InvalidXPathException ex){
			logger.error("Invalid XPath: "+ex.toString());
			halt(409, "Invalid XPath!");
		}
		
		logger.info("Creating new channel with name "+name+" and XPath "+xpath);
		channelDataAccess.addChannel(name, xpath, user);
		
		channelDataAccess.PrintAllChannels();
		
		return "Created channel!";
	}

	
}
