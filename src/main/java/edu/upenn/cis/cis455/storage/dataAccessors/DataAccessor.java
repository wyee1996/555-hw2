package edu.upenn.cis.cis455.storage.dataAccessors;

import com.sleepycat.je.DatabaseException;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;

import edu.upenn.cis.cis455.storage.databaseDAOs.SessionDAO;
import edu.upenn.cis.cis455.storage.databaseDAOs.UserDAO;

public class DataAccessor {

	// User accessor
	public PrimaryIndex<String,UserDAO> userByUsername;
	
	// Session accessor
	public PrimaryIndex<String,SessionDAO> sessionBySessionId;
	
	public DataAccessor(EntityStore store) throws DatabaseException{
		
		// primary key for user class
		userByUsername = store.getPrimaryIndex(String.class, UserDAO.class);		
		
		// primary key for session class
		sessionBySessionId = store.getPrimaryIndex(String.class, SessionDAO.class);
	}
}
