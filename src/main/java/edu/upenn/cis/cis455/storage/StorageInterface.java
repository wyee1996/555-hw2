package edu.upenn.cis.cis455.storage;

import com.sleepycat.je.Environment;
import com.sleepycat.persist.EntityStore;

import edu.upenn.cis.cis455.storage.databaseDAOs.UserDAO;

public interface StorageInterface {

	public EntityStore getEntityStore();
	
	public Environment getEnvironment();
	
    /**
     * How many documents so far?
     */
    public int getCorpusSize();

    /**
     * Add a new document, getting its ID
     */
    public int addDocument(String url, String documentContents, String contentType);

    /**
     * Retrieves a document's contents by URL
     */
    public String getDocument(String url);
    

    /**
     * Adds a user and creates a new associated session
     */
    public int addUser(String username, String password);
        
    
    /**
     * Checks if a user is logged out yet by checking user's session
     * @param username
     * @return
     */
    public boolean isUserLoggedIn(String username);

    /**
     * Tries to log in the user, or else throws a HaltException
     */
    public boolean loginUser(String username, String password);
    

    /**
     * Shuts down / flushes / closes the storage system
     */
    public void close();
}
