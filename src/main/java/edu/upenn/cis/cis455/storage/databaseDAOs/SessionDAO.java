package edu.upenn.cis.cis455.storage.databaseDAOs;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;

import com.sleepycat.je.DatabaseException;
import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

import edu.upenn.cis.cis455.storage.DatabaseEnvironment;
import edu.upenn.cis.cis455.storage.dataAccessors.DataAccessor;
import edu.upenn.cis.cis455.storage.dataAccessors.SessionDataAccessor;

@Entity
public class SessionDAO {
	
	@PrimaryKey
	private String sessionId;
	
	private Date expiresTime = new Date();
	private Date creationTime = new Date();
	private Date lastAccessedTime = new Date();	
	
		
	public String getSessionId() {
		access();
		return this.sessionId;
	}
	
	public Date getExpiresTime() {
		access();
		return this.expiresTime;
	}
	
	public Date getCreationTime() {
		access();
		return this.creationTime;
	}
	
	public Date getLastAccessedTime() {
		access();
		return this.lastAccessedTime;
	}
	
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	public void setExpiresTime(Date expiresTime) {
		this.expiresTime = expiresTime;
	}
	
	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}
	
	public void access() {
		this.lastAccessedTime = new Date();
		DateUtils.addMinutes(this.expiresTime, 5);
		DatabaseEnvironment.sessionDataAccessor.updateSession(this);
	}
	
}
