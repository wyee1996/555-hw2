package edu.upenn.cis.stormlite.bolt;

import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.crawler.Crawler;
import edu.upenn.cis.cis455.storage.dataAccessors.ChannelDataAccessor;
import edu.upenn.cis.cis455.storage.databaseDAOs.ChannelDAO;
import edu.upenn.cis.cis455.xpathengine.OccurrenceEvent;
import edu.upenn.cis.cis455.xpathengine.XPathEngine;
import edu.upenn.cis.cis455.xpathengine.XPathEngineFactory;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;

public class PathMatcherBolt implements IRichBolt{

	String executorId = UUID.randomUUID().toString();
	
	XPathEngine xPathEngine;
	
	String docBody;
	
	ArrayList<String> expressions;
	
	static final Logger logger = LogManager.getLogger(PathMatcherBolt.class);
	
	Fields myFields = new Fields();
	
	private OutputCollector collector;
	
	@Override
	public String getExecutorId() {
		return this.executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(myFields);
	}

	@Override
	public void execute(Tuple input) {
		try {
			OccurrenceEvent event = (OccurrenceEvent) input.getObjectByField("event");
			
			String newDocBody = input.getStringByField("documentBody");
			
			// if docBody has changed, then generate a new XPath engine
			if (docBody == null) {
				docBody = newDocBody;
			}
			if (!docBody.equals(newDocBody)) {
				docBody = newDocBody;
				xPathEngine = XPathEngineFactory.getXPathEngine();
				xPathEngine.setXPaths(getStringArray(expressions));
			}
			
			if (event.getType().equals(OccurrenceEvent.Type.Text) && event.getValue().equals("finished") && (event.getDepth() == 0)) {
				for (int i=0; i< expressions.size();i++) {
					if (xPathEngine.isValid(i)) {
						Crawler.db.addDocument(event.getUrl(), docBody, event.getContentType());
						
						ChannelDataAccessor channelDataAccess = new ChannelDataAccessor(Crawler.db.getEntityStore());
						channelDataAccess.addToMatchedWebPages(expressions.get(i), docBody.hashCode());
					}
				}
				System.out.println("finished processing!");
				Crawler.notifyThreadExited();
			}
			else {
				xPathEngine.evaluateEvent(event);
				//xPathEngine.printPathStatus();
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		
	}

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
			xPathEngine = XPathEngineFactory.getXPathEngine();
			expressions = Crawler.getXPathExpressions();
			xPathEngine.setXPaths(getStringArray(expressions));
	}
	
	private String[] getStringArray(ArrayList<String> strings) {
		String[] strArray = new String[strings.size()];
		for (int i=0;i<strings.size();i++) {
			strArray[i] = strings.get(i);
		}
		return strArray;
	}

	@Override
	public void setRouter(IStreamRouter router) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Fields getSchema() {
		return myFields;
	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub
		
	}

}
