package edu.upenn.cis.cis455.webinterface;

import java.util.ArrayList;

import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.storage.dataAccessors.ChannelDataAccessor;
import edu.upenn.cis.cis455.storage.databaseDAOs.ChannelDAO;
import edu.upenn.cis.cis455.storage.databaseDAOs.UserDAO;

public class BuildIndexPage {

	String beginning = "<!DOCTYPE html>\n" + 
			"<html>\n" + 
			"<body>\n" + 
			"\n";
	
	String end = "</body>\n" + 
			"</html>";
	
	private StorageInterface db;
	
	public BuildIndexPage(StorageInterface db) {
		this.db = db;
	}
	
	public String create(UserDAO user) {
		
		StringBuilder sb = new StringBuilder();
		sb.append(beginning);
		sb.append("<h1>Welcome, "+user.getUsername()+"</h1>");
		
		ArrayList<ChannelDAO> subChannels = getSubscribedChannels(user);
		for (ChannelDAO channel : subChannels) {
			sb.append("<div class=\"channelheader\">");
			sb.append("Channel name: "+"<a href=\"/show?channel="+channel.getName()+"\">"+channel.getName()+"</a>");
			sb.append("</div>");
		}
		sb.append(end);
		
		return sb.toString();
						
	}
	
	public ArrayList<ChannelDAO> getSubscribedChannels(UserDAO user){
		
		ArrayList<ChannelDAO> subscribedChannels = new ArrayList<ChannelDAO>();
		ChannelDataAccessor channelDataAccess = new ChannelDataAccessor(db.getEntityStore());
		subscribedChannels = channelDataAccess.getAllChannels();
		
		return subscribedChannels;
	}
	
}
