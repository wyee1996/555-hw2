package edu.upenn.cis.cis455.storage.dataAccessors;

import java.util.ArrayList;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sleepycat.je.LockConflictException;
import com.sleepycat.persist.EntityCursor;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.SecondaryIndex;

import edu.upenn.cis.cis455.storage.databaseDAOs.ChannelDAO;
import edu.upenn.cis.cis455.storage.databaseDAOs.UserDAO;
import edu.upenn.cis.cis455.storage.databaseDAOs.WebPageDAO;

public class ChannelDataAccessor {

	final static Logger logger = LogManager.getLogger(ChannelDataAccessor.class);
	
	EntityStore store;
	
	private PrimaryIndex<String, ChannelDAO> channelByName;
	
	private SecondaryIndex<String, String, ChannelDAO> channelByXPath;
	
	//private  SecondaryIndex<String, String, ChannelDAO> channelsByUser;
	
	//private SecondaryIndex<String, String, ChannelDAO> channelByWebPage;
	
	public ChannelDataAccessor(EntityStore store) {
		this.store = store;
		
		channelByName = store.getPrimaryIndex(String.class, ChannelDAO.class);
		
		channelByXPath = store.getSecondaryIndex(channelByName, String.class, "xPath");
		
		//channelsByUser = store.getSecondaryIndex(channelByName, String.class, "users");
		
		//channelByWebPage = store.getSecondaryIndex(channelByName, String.class, "matchedWebPages");
	}
	
	public ChannelDAO getChannel(String name) {
		return this.channelByName.get(name);
	}
		
	public void PrintAllChannels() {
		EntityCursor<ChannelDAO> channelCursor = channelByName.entities();
		System.out.println("Printing all channels");
		for (ChannelDAO channel : channelCursor) {
			System.out.println("Channel name: "+channel.getName()+" - Channel XPath: "+channel.getXPath()+" - Created by: "+channel.getCreator());
		}
	}
	
	public ArrayList<ChannelDAO> getAllChannels(){
		ArrayList<ChannelDAO> channels = new ArrayList<ChannelDAO>();
		EntityCursor<ChannelDAO> channelCursor = channelByName.entities();
		for (ChannelDAO channel : channelCursor) {
			channels.add(channel);
		}
		channelCursor.close();
		return channels;
	}
	
	public void addChannel(String name, String xpath, String user) {
		//System.out.println("Adding channel "+name+" - XPath: "+xpath);
		ChannelDAO newChannel = new ChannelDAO(name, xpath, user);
		this.channelByName.put(newChannel);
	}
	
	public void addToMatchedWebPages(String xpath, int webPageHash) throws LockConflictException{
		ChannelDAO channel = this.channelByXPath.get(xpath);
		if (channel != null){
			channel.addToMatchedWebPages(webPageHash);
			this.channelByName.put(channel);
		}
		
	}
	
	public ArrayList<WebPageDAO> getMatchedWebPages(String xpath) {
		ChannelDAO channel = this.channelByXPath.get(xpath);
		ArrayList<WebPageDAO> matchedPages = new ArrayList<WebPageDAO>();
		if (channel != null){
			ArrayList<String> webpages = channel.getMatchedWebPages();
			WebPageDataAccessor webpageDataAccess = new WebPageDataAccessor(store);
			for (String webpage : webpages) {
				System.out.println(Integer.parseInt(webpage));
				WebPageDAO webpageDAO = webpageDataAccess.webPageByHash.get(Integer.parseInt(webpage));
				if (webpageDAO != null) {
					matchedPages.add(webpageDAO);
				}
			}
		}
		return matchedPages;
	}
	
}
