package edu.upenn.cis.cis455.xpathEngine;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.upenn.cis.cis455.xpathengine.OccurrenceEvent;
import edu.upenn.cis.cis455.xpathengine.XPathEngine;
import edu.upenn.cis.cis455.xpathengine.XPathEngineFactory;

public class XPathEngineTest {

//	@Test
//	public void test() {
//		String[] expressions = new String[] {"/abc/def/xyz"};
//		XPathEngineFactory factory = new XPathEngineFactory();
//		XPathEngine xpathEngine = factory.getXPathEngine();
//		xpathEngine.setXPaths(expressions);
//		xpathEngine.printPathStatus();
//	}
	
//	@Test
//	public void test1() {
//		String[] expressions = new String[] {"/abc/def/xyz"};
//		OccurrenceEvent event = new OccurrenceEvent(OccurrenceEvent.Type.Open, "abc", "1234", 1);
//		XPathEngineFactory factory = new XPathEngineFactory();
//		XPathEngine xpathEngine = factory.getXPathEngine();
//		xpathEngine.setXPaths(expressions);
//		xpathEngine.evaluateEvent(event);
//		OccurrenceEvent event2 = new OccurrenceEvent(OccurrenceEvent.Type.Open, "def", "1234", 2);
//		xpathEngine.evaluateEvent(event2);
//		OccurrenceEvent event3 = new OccurrenceEvent(OccurrenceEvent.Type.Close, "def", "1234", 2);
//		xpathEngine.evaluateEvent(event3);
//		xpathEngine.printPathStatus();
//	}
	
//	/**
//	 * Test that engine matches on text that meets bracket conditions
//	 */
//	@Test
//	public void test2() {
//		String[] expressions = new String[] {"/abc/def/xyz[text()=\"hello\"]"};
//		OccurrenceEvent event = new OccurrenceEvent(OccurrenceEvent.Type.Open, "abc", "1234", 1);
//		XPathEngineFactory factory = new XPathEngineFactory();
//		XPathEngine xpathEngine = factory.getXPathEngine();
//		xpathEngine.setXPaths(expressions);
//		xpathEngine.evaluateEvent(event);
//		OccurrenceEvent event2 = new OccurrenceEvent(OccurrenceEvent.Type.Open, "def", "1234", 2);
//		xpathEngine.evaluateEvent(event2);
//		OccurrenceEvent event3 = new OccurrenceEvent(OccurrenceEvent.Type.Open, "xyz", "1234", 3);
//		xpathEngine.evaluateEvent(event3);
//		OccurrenceEvent event4 = new OccurrenceEvent(OccurrenceEvent.Type.Text, "hello", "1234", 3);
//		event4.setEncapsulatingElement("xyz");
//		xpathEngine.evaluateEvent(event4);
//		xpathEngine.printPathStatus();
//	}
//	
//	/**
//	 * Test that engine does not match on text that does not meet conditions
//	 */
//	@Test
//	public void test3() {
//		String[] expressions = new String[] {"/abc/def/xyz[text()=\"hello\"]"};
//		OccurrenceEvent event = new OccurrenceEvent(OccurrenceEvent.Type.Open, "abc", "1234", 1);
//		XPathEngineFactory factory = new XPathEngineFactory();
//		XPathEngine xpathEngine = factory.getXPathEngine();
//		xpathEngine.setXPaths(expressions);
//		xpathEngine.evaluateEvent(event);
//		OccurrenceEvent event2 = new OccurrenceEvent(OccurrenceEvent.Type.Open, "def", "1234", 2);
//		xpathEngine.evaluateEvent(event2);
//		OccurrenceEvent event3 = new OccurrenceEvent(OccurrenceEvent.Type.Open, "xyz", "1234", 3);
//		xpathEngine.evaluateEvent(event3);
//		OccurrenceEvent event4 = new OccurrenceEvent(OccurrenceEvent.Type.Text, "hello!", "1234", 3);
//		event4.setEncapsulatingElement("xyz");
//		xpathEngine.evaluateEvent(event4);
//		xpathEngine.printPathStatus();
//	}
	
//	/**
//	 * Test that can backtrack after matching
//	 */
//	@Test
//	public void test4() {
//		String[] expressions = new String[] {"/abc/def/xyz[text()=\"hello\"]"};
//		XPathEngineFactory factory = new XPathEngineFactory();
//		XPathEngine xpathEngine = factory.getXPathEngine();
//		xpathEngine.setXPaths(expressions);
//		OccurrenceEvent event = new OccurrenceEvent(OccurrenceEvent.Type.Open, "abc", "1234", 1);
//		xpathEngine.evaluateEvent(event);
//		OccurrenceEvent event2 = new OccurrenceEvent(OccurrenceEvent.Type.Open, "def", "1234", 2);
//		xpathEngine.evaluateEvent(event2);
//		OccurrenceEvent event3 = new OccurrenceEvent(OccurrenceEvent.Type.Open, "xyz", "1234", 3);
//		xpathEngine.evaluateEvent(event3);
//		OccurrenceEvent event4 = new OccurrenceEvent(OccurrenceEvent.Type.Text, "hello!", "1234", 3);
//		event4.setEncapsulatingElement("xyz");
//		xpathEngine.evaluateEvent(event4);
//		OccurrenceEvent event5 = new OccurrenceEvent(OccurrenceEvent.Type.Close, "xyz", "1234", 3);
//		xpathEngine.evaluateEvent(event5);
//		OccurrenceEvent event6 = new OccurrenceEvent(OccurrenceEvent.Type.Close, "def", "1234", 2);
//		xpathEngine.evaluateEvent(event6);
//		OccurrenceEvent event7 = new OccurrenceEvent(OccurrenceEvent.Type.Close, "abc", "1234", 1);
//		xpathEngine.evaluateEvent(event7);
//		xpathEngine.printPathStatus();
//	}
	
//	/**
//	 * Test that can match on documents w/ parent-child nodes that are same
//	 */
//	@Test
//	public void test5() {
//		String[] expressions = new String[] {"/abc/abc"};
//		XPathEngineFactory factory = new XPathEngineFactory();
//		XPathEngine xpathEngine = factory.getXPathEngine();
//		xpathEngine.setXPaths(expressions);
//		xpathEngine.printPathStatus();
//		OccurrenceEvent event = new OccurrenceEvent(OccurrenceEvent.Type.Open, "abc", 1);
//		xpathEngine.evaluateEvent(event);
//		xpathEngine.printPathStatus();
//		OccurrenceEvent event2 = new OccurrenceEvent(OccurrenceEvent.Type.Open, "abc",  2);
//		xpathEngine.evaluateEvent(event2);
//		xpathEngine.printPathStatus();
//		OccurrenceEvent event3 = new OccurrenceEvent(OccurrenceEvent.Type.Open, "xyz",  3);
//		xpathEngine.evaluateEvent(event3);
//		xpathEngine.printPathStatus();
//		OccurrenceEvent event4 = new OccurrenceEvent(OccurrenceEvent.Type.Text, "hello",  3);
//		event4.setEncapsulatingElement("xyz");
//		xpathEngine.evaluateEvent(event4);
//		OccurrenceEvent event5 = new OccurrenceEvent(OccurrenceEvent.Type.Close, "xyz",  3);
//		xpathEngine.evaluateEvent(event5);
//		OccurrenceEvent event6 = new OccurrenceEvent(OccurrenceEvent.Type.Close, "abc",  2);
//		xpathEngine.evaluateEvent(event6);
//		OccurrenceEvent event7 = new OccurrenceEvent(OccurrenceEvent.Type.Close, "abc", 1);
//		xpathEngine.evaluateEvent(event7);
//		xpathEngine.printPathStatus();
//	}
	
//	/**
//	 * Test that we can match 3 expressions
//	 */
//	@Test
//	public void test6() {
//		String[] expressions = new String[] {"/abc/def/xyz[text()=\"hello\"]","/abc/xyz/nbc","/abc/nyc"};
//		XPathEngineFactory factory = new XPathEngineFactory();
//		XPathEngine xpathEngine = factory.getXPathEngine();
//		xpathEngine.setXPaths(expressions);
//		OccurrenceEvent event = new OccurrenceEvent(OccurrenceEvent.Type.Open, "abc", "1234", 1);
//		xpathEngine.evaluateEvent(event);
//		OccurrenceEvent event2 = new OccurrenceEvent(OccurrenceEvent.Type.Open, "def", "1234", 2);
//		xpathEngine.evaluateEvent(event2);
//		OccurrenceEvent event3 = new OccurrenceEvent(OccurrenceEvent.Type.Open, "xyz", "1234", 3);
//		xpathEngine.evaluateEvent(event3);
//		OccurrenceEvent event4 = new OccurrenceEvent(OccurrenceEvent.Type.Text, "hello", "1234", 3);
//		event4.setEncapsulatingElement("xyz");
//		xpathEngine.evaluateEvent(event4);
//		OccurrenceEvent event5 = new OccurrenceEvent(OccurrenceEvent.Type.Close, "xyz", "1234", 3);
//		xpathEngine.evaluateEvent(event5);
//		OccurrenceEvent event6 = new OccurrenceEvent(OccurrenceEvent.Type.Close, "def", "1234", 2);
//		xpathEngine.evaluateEvent(event6);
//		OccurrenceEvent event7 = new OccurrenceEvent(OccurrenceEvent.Type.Open, "nyc", "1234", 2);
//		xpathEngine.evaluateEvent(event7);
//		OccurrenceEvent event8 = new OccurrenceEvent(OccurrenceEvent.Type.Close, "nyc", "1234", 2);
//		xpathEngine.evaluateEvent(event8);
//		OccurrenceEvent event9 = new OccurrenceEvent(OccurrenceEvent.Type.Close, "abc", "1234", 1);
//		xpathEngine.evaluateEvent(event9);
//		xpathEngine.printPathStatus();
//	}

}
