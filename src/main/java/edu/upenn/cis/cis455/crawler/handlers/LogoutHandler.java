package edu.upenn.cis.cis455.crawler.handlers;

import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.storage.dataAccessors.SessionDataAccessor;
import edu.upenn.cis.cis455.storage.dataAccessors.UserDataAccessor;

import static spark.Spark.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import spark.Request;
import spark.Response;
import spark.Route;
import spark.Session;

public class LogoutHandler implements Route{
	Logger logger = LogManager.getLogger(LogoutHandler.class);
	
	StorageInterface db;
	
	public LogoutHandler(StorageInterface db) {
		this.db = db;
	}
	
	@Override
	public Object handle(Request request, Response response) throws Exception {
		
//		if (response.status() - 300 < 100) {
//			return null;
//		}
		Session session = request.session(false);
		if (session != null) {
			logger.info("Invalidating session");
			session.invalidate();
		}
		else {
			logger.info("Unable to find session for this request!");
		}
		
		//
		logger.info("Logging out");
		response.redirect("/login-form.html");
		return "Logged out";
		
		//SessionDataAccessor sessionDataAccessor = new SessionDataAccessor(db.getEntityStore());
        
		//UserDataAccessor userDataAccessor = new UserDataAccessor(db.getEntityStore());
		//UserDAO user = userDataAccessor.getUserForUsername(username)
	}
	
	
	
}
