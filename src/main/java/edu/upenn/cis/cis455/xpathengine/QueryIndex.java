package edu.upenn.cis.cis455.xpathengine;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Set;

public class QueryIndex {

	Hashtable<String, QueryIndexLists> queryIndex = new Hashtable<String, QueryIndexLists>();
		
	public void addToCandidateList(String element, PathNode node) {
		if (queryIndex.containsKey(element)) {
			queryIndex.get(element).addToCandList(node);
		}
		else {
			QueryIndexLists queryIndexList = new QueryIndexLists();
			queryIndexList.addToCandList(node);
			queryIndex.put(element, queryIndexList);
		}
	}
	
	public void addToWaitList(String element, PathNode node) {
		if (queryIndex.containsKey(element)) {
			queryIndex.get(element).addToWaitList(node);
		}
		else {
			QueryIndexLists queryIndexList = new QueryIndexLists();
			queryIndexList.addToWaitList(node);
			queryIndex.put(element, queryIndexList);
		}
	}
	
	public void promote(String element, PathNode node) {
		if (queryIndex.containsKey(element)) {
			queryIndex.get(element).promote(node);
		}
	}
	
	public void deleteFromCandList(String element, PathNode node) {
		if (queryIndex.containsKey(element)) {
			queryIndex.get(element).deleteFromCandList(node);
		}
	}
	
	public void deleteFromWaitList(String element, PathNode node) {
		if (queryIndex.containsKey(element)) {
			queryIndex.get(element).deleteFromWaitList(node);
		}
	}
	
	public ArrayList<PathNode> getCandList(String elementStr){
		if (queryIndex.containsKey(elementStr)) {
			return queryIndex.get(elementStr).getCandList();
		}
		return new ArrayList<PathNode>();
	}
	
	/**
	 * Checks if a given element string and position match any current paths in the candidate list
	 * If so, then 
	 * @param elementStr
	 * @param position
	 * @return
	 */
	public boolean handleOpenEvent(String elementStr, int position) {
		if (queryIndex.containsKey(elementStr)) {
			return queryIndex.get(elementStr).candListMatches(elementStr, position);
		}
		return false;
	}
	
	public void printQueryIndex(String eventDesc) {
		Set<String> set = queryIndex.keySet();
		StringBuilder sb = new StringBuilder();
		sb.append("\n"+eventDesc);
		for (String key : set) {
			ArrayList<PathNode> cl = queryIndex.get(key).getCandList();
			ArrayList<PathNode> wl = queryIndex.get(key).getWaitList();
			
			sb.append("\nElement: "+key);
			sb.append(" - CL: ");
			for (PathNode cand : cl) {
				sb.append(cand.getQueryId()+"|"+cand.getNodeName()+" , ");
			}
			sb.append(" - WL: ");
			for (PathNode wait : wl) {
				sb.append(wait.getQueryId()+"|"+wait.getNodeName()+" , ");
			}
		}
		System.out.println(sb.toString());
	}
}
