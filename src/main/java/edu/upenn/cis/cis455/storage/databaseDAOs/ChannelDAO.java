package edu.upenn.cis.cis455.storage.databaseDAOs;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.sleepycat.je.LockConflictException;
import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;
import com.sleepycat.persist.model.Relationship;
import com.sleepycat.persist.model.SecondaryKey;

@Entity
public class ChannelDAO {

	@PrimaryKey
	private String name;
	
	@SecondaryKey(relate=Relationship.ONE_TO_ONE)
	private String xPath;
	
//	@SecondaryKey(relate=Relationship.MANY_TO_MANY, relatedEntity=UserDAO.class)
//	private Set<String> users = new HashSet<String>();
	
	private String creator;
	
	//@SecondaryKey(relate=Relationship.ONE_TO_MANY)
//	private String matchedWebPage;
	private ArrayList<String> matchedWebPages = new ArrayList<String>();
	
	private Date crawledOnTime;
	
	public ChannelDAO() {}
	
	public ChannelDAO(String name, String xPath, String creator) {
		this.name = name;
		this.xPath = xPath;
		this.creator = creator;
		System.out.println("Created channel");
	}
	
	public void addToMatchedWebPages(Integer webPageHash) throws LockConflictException{
		matchedWebPages.add(webPageHash.toString());
	}
//	
	public ArrayList<String> getMatchedWebPages(){
		return this.matchedWebPages;
	}
	
	public void setCrawledOnTime(Date time) {
		this.crawledOnTime = time;
	}
	
	public Date getCrawledOnTime() {
		return this.crawledOnTime;
	}
	
	public String getCreator() {
		return this.creator;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getXPath() {
		return this.xPath;
	}
	
}
