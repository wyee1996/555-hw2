package edu.upenn.cis.cis455.webCrawlerTests;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;

import org.junit.Before;
import org.junit.Test;

import edu.upenn.cis.cis455.crawler.HttpResponseParser;
import edu.upenn.cis.cis455.crawler.RobotsTxtHandler;
import edu.upenn.cis.cis455.crawler.utils.RobotsTxtInfo;
import edu.upenn.cis.cis455.databaseTests.TestHelper;

public class HttpResponseParserTest {

	String response1 = "HTTP/1.1 200 OK\r\n" + 
			"Date: Tue, 27 Oct 2020 20:10:08 GMT\r\n" + 
			"Server: Apache\r\n" + 
			"Last-Modified: Tue, 13 Feb 2018 10:47:35 GMT\r\n" + 
			"ETag: \"135-56515b8f3abec\"\r\n" + 
			"Accept-Ranges: bytes\r\n" + 
			"Content-Length: 309\r\n" + 
			"Content-Type: text/plain\r\n" + 
			"\r\n" + 
			"# These defaults shouldn't apply to your crawler\r\n" + 
			"User-agent: *\r\n" + 
			"Disallow: /marie/\r\n" + 
			"Crawl-delay: 10\r\n" + 
			"\r\n" + 
			"# Below is the directive your crawler should use:\r\n" + 
			"User-agent: cis455crawler\r\n" + 
			"Disallow: /marie/private/\r\n" + 
			"Disallow: /foo/\r\n" + 
			"Crawl-delay: 5\r\n" + 
			"\n" + 
			"# This should be ignored by your crawler\r\n" + 
			"User-agent: evilcrawler\r\n" + 
			"Disallow: /\r\n";
	Socket socket;
	
	@Test
	public void test() throws IOException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		socket = TestHelper.getMockSocket(response1, output);
		Hashtable<String, String> headers = new Hashtable<String,String>();
		BufferedReader reader = HttpResponseParser.parseResponse(socket.getInputStream(), headers);
		
		RobotsTxtInfo robotsTxt = RobotsTxtHandler.parse(reader);
		
//		ArrayList<String> allDisallowed = new ArrayList<String>(Arrays.asList("/marie/"));
//		ArrayList<String> crawlerDisallowed = new ArrayList<String>(Arrays.asList("/marie/private/","/foo/"));
//		ArrayList<String> evilcrawlerDisallowed = new ArrayList<String>(Arrays.asList("/"));
//		
//		ArrayList<String> userAgents = new ArrayList<String>(Arrays.asList("*","cis455crawler","evilcrawler"));
//		int allCrawlDelay = 10;
//		int crawlerDelay = 5;
//		
//		assertTrue(robotsTxt.getDisallowedLinks("*").equals(allDisallowed));
//		assertTrue(robotsTxt.getDisallowedLinks("cis455crawler").equals(o))
		robotsTxt.print();
	}
	
	

}
