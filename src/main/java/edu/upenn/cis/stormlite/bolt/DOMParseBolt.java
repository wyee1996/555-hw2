package edu.upenn.cis.stormlite.bolt;

import java.io.StringReader;
import java.util.Map;
import java.util.UUID;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;
import org.xml.sax.InputSource;

import edu.upenn.cis.cis455.crawler.Crawler;
import edu.upenn.cis.cis455.xpathengine.DocumentInfo;
import edu.upenn.cis.cis455.xpathengine.OccurrenceEvent;
import edu.upenn.cis.cis455.xpathengine.SAXParseHandler;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;

public class DOMParseBolt implements IRichBolt{

	String executorId = UUID.randomUUID().toString();
	
	static final Logger logger = LogManager.getLogger(DOMParseBolt.class);
	
	Fields myFields = new Fields("documentBody","event");
	
	private OutputCollector collector;
	
	@Override
	public String getExecutorId() {
		return this.executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(myFields);
	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void execute(Tuple input) {
		
		// Take in a documentBody
		//DocumentBody docBody = (DocumentBody)input.getObjectByField("documentBody");
		//String documentBody = docBody.getDocumentBody();
		DocumentInfo docInfo = (DocumentInfo)input.getObjectByField("documentInfo");
		String documentBody = docInfo.getDocumentBody();
		
		try {
			// Parse documentBody for startElement, endElement, and text in between
			
			// SAXParseHandler emits elements as it encounters them
//			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
//			SAXParser saxParser = saxParserFactory.newSAXParser();
//			SAXParseHandler handler = new SAXParseHandler(this.collector);
//			saxParser.parse(new InputSource(new StringReader(documentBody)), handler);	
			Crawler.setWorking(true);
			Document doc = Jsoup.parse(documentBody);
			convertLinksToAbs(doc, docInfo);
			Elements all = doc.getAllElements();
			traverse(all.first(), doc.body().toString());
			docInfo.setDocumentBody(doc.body().toString());
			notifyDone(docInfo);
		}
		catch (Exception e) {
			System.out.println("Error parsing document "+documentBody.substring(0, 10));
			e.printStackTrace();
		}
		Thread.yield();
	}
	
	/**
	 * Traverse through the DOM tree and output open, text, and close elements
	 * This does DFS
	 * @param root
	 */
	public void traverse(Element root, String documentBody) {
        Element node = root;
        int depth = 1;
        boolean outputtedText = false;
        int numChildNodes = node.childNodeSize();
        while (node != null) {
            if (node.nodeName().equals("#document")){
            	node = node.child(0);
            	continue;
            }
            if (!outputtedText) {
            	head(node, documentBody, depth);
        	}
        	else outputtedText = false;
            
            if (numChildNodes > 0) {
            	try {
            		node = node.child(0);
            		numChildNodes = node.childNodeSize();
            		depth++;
            	}
            	catch (Exception e){
            		numChildNodes = 0;
            		Node newNode = node.childNode(0);
            		text(newNode, documentBody, depth, node.nodeName());
            		outputtedText = true;
            	}
                
            } else {
                while (node.nextElementSibling() == null && depth > 1) {
                    tail(node, documentBody, depth);
                    node = node.parent();
                    numChildNodes = node.childNodeSize();
                    depth--;
                }
                tail(node, documentBody, depth);
                if (node == root)
                    break;
                node = node.nextElementSibling();
                numChildNodes = getChildNodeSize(node);
            }
        }
    }
	
	private int getChildNodeSize(Element node) {
		try {
			return node.childNodeSize();
		} catch(Exception ex) {
			return 0;
		}
	}
	
	public void head(Element node, String docBody, int depth) {
		if (!node.nodeName().equals("#document")) {
			OccurrenceEvent event = new OccurrenceEvent(OccurrenceEvent.Type.Open, node.nodeName(), depth);
			collector.emit(new Values<Object>(docBody, event));
		}
		
		//System.out.println("DOMParse emitting open: "+node.nodeName()+" - depth "+depth);
	}
	
	private void tail(Element node, String docBody, int depth) {
		if (!node.nodeName().equals("#document")) {
			OccurrenceEvent event = new OccurrenceEvent(OccurrenceEvent.Type.Close, node.nodeName(), depth);
			collector.emit(new Values<Object>(docBody, event));
		}
		
		//System.out.println("DOMParse emitting close: "+node.nodeName()+" - depth "+depth);
	}
	
	private void text(Node node, String docBody, int depth, String encapsulatingElement) {
		OccurrenceEvent event = new OccurrenceEvent(OccurrenceEvent.Type.Text, node.toString(), depth);
		event.setEncapsulatingElement(encapsulatingElement);
		collector.emit(new Values<Object>(docBody, event));
		//System.out.println("DOMParse emitting text: "+node.toString()+" - depth "+depth);
	}
	
	private void notifyDone(DocumentInfo docInfo) {
		OccurrenceEvent event = new OccurrenceEvent(OccurrenceEvent.Type.Text, "finished", 0);
		event.setUrl(docInfo.getUrl());
		event.setContentType(docInfo.getType());
		collector.emit(new Values<Object>(docInfo.getDocumentBody(), event));
		System.out.println("DOMParse finished traversing document!");
	}
	
	private void convertLinksToAbs(Document doc, DocumentInfo docInfo) {
		Elements links = doc.select("a");
		String baseUri = docInfo.getHostname();
		for (Element link : links) {
			if (!link.attr("href").toLowerCase().startsWith("http")) {
				//link.replaceWith(in);
				String newhref = baseUri+"/"+link.attr("href");
				link.removeAttr("href");
				link.attr("href",newhref);
				System.out.println(link.toString());
			}
			//link.setBaseUri(baseUri);
		}
	}

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		this.collector = collector;
		
	}

	@Override
	public void setRouter(IStreamRouter router) {
		this.collector.setRouter(router);
	}

	@Override
	public Fields getSchema() {
		return myFields;
	}


}
