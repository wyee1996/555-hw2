package edu.upenn.cis.cis455.crawler.handlers;

import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.storage.dataAccessors.UserDataAccessor;
import edu.upenn.cis.cis455.storage.databaseDAOs.UserDAO;
import static spark.Spark.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import spark.Request;
import spark.Response;
import spark.Route;
import spark.Session;

public class RegistrationHandler implements Route{
	final static Logger logger = LogManager.getLogger(RegistrationHandler.class);
	StorageInterface db;

    public RegistrationHandler(StorageInterface db) {
        this.db = db;
    }

	@Override
	public Object handle(Request request, Response response) throws Exception {
		String user = request.queryParams("username");
        String pass = request.queryParams("password");
        
        UserDataAccessor userDataAccessor = new UserDataAccessor(db.getEntityStore());
        UserDAO existingUser = userDataAccessor.getUserForUsername(user);
        if (existingUser != null) {
        	halt(409, "Username is unavailable");
        }
        
        Session session = request.session(true);
        session.maxInactiveInterval(5*60);
        session.attribute("user", user);
        session.attribute("password", pass); 
        
        db.addUser(user, pass);
        logger.info("Adding user "+user+" with session id "+session.id());
        //userDataAccessor.updateUser(userDAO);
        logger.info("Registered user "+user);
        //logger.info("Redirecting user to /index.html");
        //response.redirect("/index.html");
        //String returnString = 
        return "<!DOCTYPE html>\n" + 
		"<html>\n" + 
		"<body>\n" + 
		"\n" + 
		"<h1>Hello!</h1>"+
		"<button type=\"button\" onclick=\"window.location.href='/index.html';\">Main Page</button>"+
				"\n" + 
				"</body>\n" + 
				"</html>";
	}
    
    
}
