package edu.upenn.cis.cis455.crawler.handlers;

import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.storage.dataAccessors.ChannelDataAccessor;
import edu.upenn.cis.cis455.storage.databaseDAOs.ChannelDAO;
import edu.upenn.cis.cis455.storage.databaseDAOs.WebPageDAO;
import spark.Request;
import spark.Response;
import spark.Route;
import static spark.Spark.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Set;

public class GetChannelHandler implements Route{

	StorageInterface db;
	
	String beginning = "<!DOCTYPE html>\n" + 
			"<html>\n" + 
			"<body>\n" + 
			"\n";
	
	String end = "</body>\n" + 
			"</html>";
	
	String channelBeginning = "<div class=\"channelheader\">Channel name: ";
	String channelEnd = "</div class=\"channelheader\">";
	String createdBy = ", created by: ";
	String crawledOn = "Crawled on: ";
	String location = " Location: ";
	String docBeginning = "<div class=\"document\">";
	String docEnd = "</document>";
	
	String creator;
	String channelName;
	
	public GetChannelHandler(StorageInterface db) {
		this.db = db;
	}
	
	@Override
	public Object handle(Request request, Response response) throws Exception {
		channelName = request.queryParams("channel");
		
		ChannelDataAccessor channelDataAccess = new ChannelDataAccessor(db.getEntityStore());
		ChannelDAO channel = channelDataAccess.getChannel(channelName);
		if (channel == null) halt(404,"Channel doesn't exist!");
		
		creator = channel.getCreator();
		
		ArrayList<WebPageDAO> matchedPages = channelDataAccess.getMatchedWebPages(channel.getXPath());
		
		return buildChannelPage(matchedPages);
	}
	
	private String buildChannelPage(ArrayList<WebPageDAO> webpages) {
		StringBuilder sb = new StringBuilder();
		sb.append(channelBeginning);
		sb.append(channelName);
		sb.append(createdBy);
		sb.append(creator);
		for (WebPageDAO webpage : webpages) {
			sb.append(docBeginning);
			sb.append(crawledOn);
			String date = new SimpleDateFormat("YYYY-MM-DD").format(webpage.getTimeLastChecked())+"T"
					+new SimpleDateFormat("hh:mm:ss").format(webpage.getTimeLastChecked());
			sb.append(date);
			sb.append(location);
			Set<String> links = webpage.getLinks();
			sb.append(links.iterator().next());
			sb.append(webpage.getWebPageContents());
			sb.append(docEnd);
		}		
		sb.append(channelEnd);
		return sb.toString();
	}

}
