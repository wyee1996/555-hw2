package edu.upenn.cis.stormlite.spout;

import java.util.Map;
import java.util.UUID;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Values;

public class TestSpout implements IRichSpout{

	String executorId = UUID.randomUUID().toString();
	
	SpoutOutputCollector collector;
	
	@Override
	public String getExecutorId() {
		return executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("documentBody"));
		
	}

	@Override
	public void open(Map<String, String> config, TopologyContext topo, SpoutOutputCollector collector) {
		this.collector = collector;
		
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void nextTuple() {
		String doc = "<!DOCTYPE html>\n" + 
				"<html>\n" + 
				"<body>\n" + 
				"\n" + 
				"<h1>My First Heading</h1>\n" + 
				"<p>My first paragraph.</p>\n" + 
				"\n" + 
				"</body>\n" + 
				"</html>";
		collector.emit(new Values<Object>(doc));
		Thread.yield();
	}

	@Override
	public void setRouter(IStreamRouter router) {
		this.collector.setRouter(router);	
		
	}

}
