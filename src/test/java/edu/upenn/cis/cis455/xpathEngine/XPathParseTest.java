package edu.upenn.cis.cis455.xpathEngine;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.upenn.cis.cis455.xpathengine.InvalidXPathException;
import edu.upenn.cis.cis455.xpathengine.PathNode;
import edu.upenn.cis.cis455.xpathengine.XPathParser;

public class XPathParseTest {
	
	XPathParser parser = new XPathParser(1);

	public void printNode(PathNode node) {
		node.print();
		while(node.getChild() != null) {
			node = node.getChild();
			node.print();
		}
	}
	
	@Test
	public void test() {
		String expression = "/abc/def/xyz";
		try {
			PathNode node = parser.parse(expression);
			
			printNode(node);
		} catch (InvalidXPathException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void test1() {
		String expression = "/def[text()=\"hello\"]/abc";
		try {
			PathNode node = parser.parse(expression);
			
			printNode(node);
		} catch (InvalidXPathException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
//	
	@Test
	public void test2() {
		String expression = "/xyz/abc[contains(text(),\"someSubstring\")]";
		try {
			PathNode node = parser.parse(expression);
			printNode(node);
		} catch (InvalidXPathException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
//	
	@Test
	public void test3() {
		String expression = "/d/e/f/foo[text()=\"something\"]/bar";
		try {
			PathNode node = parser.parse(expression);
			printNode(node);
		} catch (InvalidXPathException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
//	
	@Test
	public void test4() {
		String expression = "/def[text()=\"hello=\"test\"\"]/abc";
		try {
			PathNode node = parser.parse(expression);
			printNode(node);
		} catch (InvalidXPathException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void test5() {
		String expression = "/a/b/c[text()   =     \"whiteSpacesShouldNotMatter\"]";
		try {
			PathNode node = parser.parse(expression);
			printNode(node);
		} catch (InvalidXPathException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void test6() {
		String expression = "/a/b/c[t ex t()   =     \"whiteSpacesShouldNotMatter\"]";
		try {
			PathNode path = parser.parse(expression);
		} catch (InvalidXPathException e) {
			//assertTrue(e.getMessage().equals("text()=\"...\" bracket expression is invalid"));
			assertTrue(true);
		}
	}
	
	@Test
	public void test7() {
		String expression = "/a/b/c [text(   )   =     \"whiteSpacesShouldNotMatter\"]";
		try {
			PathNode path = parser.parse(expression);
			printNode(path);
		} catch (InvalidXPathException e) {
			e.printStackTrace();
		}
	}
//	
	@Test
	public void test8() {
		String expression = "/a/b/ [text(   )   =     \"whiteSpacesShouldNotMatter\"]";
		try {
			parser.parse(expression);
		} catch (InvalidXPathException e) {
			assertTrue(true);
		}
	}
//	
	@Test
	public void test9() {
		String expression = "/a/b/";
		try {
			parser.parse(expression);
		} catch (InvalidXPathException e) {
			assertTrue(true);
		}
		
	}
	
	@Test
	public void test10() {
		String expression = "/a/a";
		try {
			PathNode node = parser.parse(expression);
			printNode(node);
		} catch (InvalidXPathException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
