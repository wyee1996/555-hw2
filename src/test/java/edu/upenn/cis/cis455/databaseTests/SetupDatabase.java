package edu.upenn.cis.cis455.databaseTests;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;


public final class SetupDatabase {
	
	public static void purgeDatabase(String homeDirectory) throws IOException {
		// Delete the testDatabase if it exists
		File file = new File(homeDirectory);
		if (file.exists()) {
			for(File f: file.listFiles()) {
				f.delete();
			}
			Files.deleteIfExists(file.toPath());	
		}		
	}
}
