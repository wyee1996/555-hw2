package edu.upenn.cis.cis455.crawler.handlers;

import spark.Request;
import spark.Route;
import spark.Response;
import spark.HaltException;
import spark.Session;
import static spark.Spark.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.storage.dataAccessors.SessionDataAccessor;

public class LoginHandler implements Route {
    StorageInterface db;
    Logger logger = LogManager.getLogger(LoginHandler.class);

    public LoginHandler(StorageInterface db) {
        this.db = db;
    }

    @Override
    public String handle(Request req, Response resp) throws HaltException {
        String user = req.queryParams("username");
        String pass = req.queryParams("password");
        
        if (user == null || pass == null) {
        	halt(400, "Query parameters are invalid!");
        }

        System.err.println("Login request for " + user + " and " + pass);
        if (db.loginUser(user, pass)) {
            System.err.println("Logged in to user "+user);
            
            logger.info("Creating a new session for this request");
            Session session = req.session();
            
            session.maxInactiveInterval(5*60);
            session.attribute("user", user);
            session.attribute("password", pass);            
            
            //resp.cookie("JSESSIONID", session.id(),300);
            //logger.info("Redirecting to /index.html");
            //resp.redirect("/index.html");
        } else {
            System.err.println("Invalid credentials");
            halt(401, "Invalid credentials");
            //resp.redirect("/login-form-invalid.html");
        }

        return "";
    }
}
