package edu.upenn.cis.stormlite.bolt;

import java.io.BufferedReader;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.crawler.Crawler;
import edu.upenn.cis.cis455.crawler.PageParser;
import edu.upenn.cis.cis455.crawler.RobotsTxtHandler;
import edu.upenn.cis.cis455.crawler.utils.RobotsTxtInfo;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.xpathengine.DocumentInfo;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;

public class DocFetcherBolt implements IRichBolt{

	String executorId = UUID.randomUUID().toString();
	
	static final Logger logger = LogManager.getLogger(DocFetcherBolt.class);
	
	Fields myFields = new Fields("hostname","documentInfo");
	
	private OutputCollector collector;
	
	public DocFetcherBolt() {}
	
	@Override
	public String getExecutorId() {
		return this.executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(myFields);
	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void execute(Tuple input) {
		
		URLInfo link = (URLInfo) input.getObjectByField("url");
		
		System.out.println(getExecutorId() + ": starting execution on url "+link.toString());
		
		RobotsTxtInfo robotsInfo;
		
		try {			
			// Check robots.txt
			if (!Crawler.robotsForHostname.containsKey(link.getHostName())) {
				
				// check if link has a robots.txt
				BufferedReader reader = RobotsTxtHandler.getRobotsTxt(link.toString()+"/robots.txt");
				if (reader != null) {
					robotsInfo = RobotsTxtHandler.parse(reader);
				}
				else {
					// if doesn't exist, then no restrictions
					robotsInfo = new RobotsTxtInfo();
				}
				
				Crawler.robotsForHostname.put(link.getHostName(), robotsInfo);
			}
			else {
				robotsInfo = Crawler.robotsForHostname.get(link.getHostName());
			}
			
			// make sure that crawler is allowed crawl the link. If not, then skip it
			if (Crawler.isOKtoCrawl(link)) return;
			
			// make sure that we are following crawl delay directive. 
			Integer crawlDelay = Crawler.deferCrawl(link);
			if (crawlDelay != null) {
				Thread.sleep(crawlDelay*1000);
			}
			
			// send HEAD request to ensure content type and length are within our bounds
			if (!PageParser.checkHead(link)) return;
			
			
			String documentBody = PageParser.downloadDocumentFromUrl(link.toString());
			DocumentInfo documentInfo = new DocumentInfo(documentBody, link.toString(), link.getContentType(), link.toStringNoPath());
			
			Crawler.incCount();
			System.out.println("Downloaded document "+documentBody.substring(0, 10)+"... and now emitting");
			collector.emit(new Values<Object>(link.toString(), documentInfo));
			
		} catch (Exception e) {
			logger.error(getExecutorId() + ": error fetching document at url "+link.toString());
			e.printStackTrace();
		}
	}

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		this.collector = collector;
		
	}

	@Override
	public void setRouter(IStreamRouter router) {
		this.collector.setRouter(router);
	}

	@Override
	public Fields getSchema() {
		return myFields;
	}

}
