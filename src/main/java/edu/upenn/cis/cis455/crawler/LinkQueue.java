package edu.upenn.cis.cis455.crawler;

import java.util.LinkedList;
import java.util.Queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.crawler.utils.URLInfo;

public class LinkQueue {
	final static Logger logger = LogManager.getLogger(LinkQueue.class);
	
	private final Queue<URLInfo> _linkQueue = new LinkedList<URLInfo>();
	private int _maxNumTasks;
	private boolean isClosed = false;
	
	public LinkQueue (int maxNumTasks){
		this._maxNumTasks = maxNumTasks;
	}
	
	public void addLink (URLInfo link) throws InterruptedException {
		while(true) {
			synchronized (_linkQueue) {
				if (_linkQueue.size() == _maxNumTasks) {
					//logger.debug("Queue full");
					_linkQueue.wait();
				}
				else{
					_linkQueue.add(link);
					//logger.debug("Added link to queue");
					_linkQueue.notifyAll();
					break;
				}
			}
		}
		
	}
	
	public URLInfo popLink() throws InterruptedException {
		while(true) {
			synchronized(_linkQueue) {
//				if(_linkQueue.isEmpty()) {
//					//logger.debug("Queue currently empty");
//					_linkQueue.wait();
//				}
				//else {
					//logger.debug("Grabbing link from queue");
					URLInfo link =  _linkQueue.poll();
					//logger.debug("Grabbed link from queue");
					_linkQueue.notifyAll();
					return link;
				//}
			}
		}
		//return null;
	}
		
	public boolean isEmpty() {
		if (_linkQueue.size() == 0) {
			return true;
		}
		return false;
	}
	
	public int getSize() {
		return _linkQueue.size();
	}
	
	public void close() {
		isClosed = true;
	}
}
