package edu.upenn.cis.cis455.storage;

import java.io.File;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

public class StorageFactory {
    public static StorageInterface getDatabaseInstance(String directoryPath, boolean isReadOnly) {
    	
    	//URL url = URL.class.getResource(directory);
    	
    	
    	File directory = new File(directoryPath);
    	
    	// Creates the directory if it doesn't currently exist
    	if (!directory.exists()) {
    		directory.mkdir();
    	}
    	
        DatabaseEnvironment env = new DatabaseEnvironment();
        env.setup(directory, isReadOnly);
    	return env;
    }
}
