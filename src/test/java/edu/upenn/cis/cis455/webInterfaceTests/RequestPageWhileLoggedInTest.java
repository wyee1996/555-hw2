package edu.upenn.cis.cis455.webInterfaceTests;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

import edu.upenn.cis.cis455.databaseTests.SetupDatabase;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;

/**
 * Tests functionality when user is logged in (has valid Session) and makes requests
 * @author vagrant
 *
 */
public class RequestPageWhileLoggedInTest {

	String homeDirectory = "/vagrant/testDatabase";
	StorageInterface database;
	
//	@BeforeClass
//	public void before() throws IOException {
//		SetupDatabase.purgeDatabase(homeDirectory);
//		database = StorageFactory.getDatabaseInstance(homeDirectory, false);
//		database.addUser("testUser", "pass123","session123");
//	}
	
	// Test1: User is logged in and makes request to root URL
		// Returns 200 OK w/ "Welcome <user>"
	@Test
	public void test() {
		//fail("Not yet implemented");
	}
	
	// Test2: User is logged in and makes request to /index.html
				// Returns 200 OK w/ "Welcome <user>"
		// Test3: User is logged in and makes request to path or directory that doesn't exist
				// Returns 404 error
		// Test4: User is logged in and makes request to path that does exist
				// Returns 200 OK w/ required files

}
