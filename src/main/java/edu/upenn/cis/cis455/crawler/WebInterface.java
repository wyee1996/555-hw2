package edu.upenn.cis.cis455.crawler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.server.Response;
import org.jsoup.Connection.Method;

import static spark.Spark.*;

import edu.upenn.cis.cis455.crawler.handlers.CreateChannelHandler;
import edu.upenn.cis.cis455.crawler.handlers.GetChannelHandler;
import edu.upenn.cis.cis455.crawler.handlers.GetFileFromDBHandler;
import edu.upenn.cis.cis455.crawler.handlers.GetIndexHandler;
import edu.upenn.cis.cis455.crawler.handlers.LoginFilter;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.storage.dataAccessors.WebPageDataAccessor;
import edu.upenn.cis.cis455.storage.databaseDAOs.WebPageDAO;
import edu.upenn.cis.cis455.crawler.handlers.LoginHandler;
import edu.upenn.cis.cis455.crawler.handlers.LogoutHandler;
import edu.upenn.cis.cis455.crawler.handlers.PageNotFoundHandler;
import edu.upenn.cis.cis455.crawler.handlers.RegistrationHandler;

public class WebInterface {
	final static Logger logger = LogManager.getLogger(WebInterface.class);

	
    public static void main(String args[]) {
    	org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
        if (args.length < 1 || args.length > 2) {
            System.out.println("Syntax: WebInterface {path} {root}");
            System.exit(1);
        }

        if (!Files.exists(Paths.get(args[0]))) {
            try {
                Files.createDirectory(Paths.get(args[0]));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        port(45555);
        StorageInterface database = StorageFactory.getDatabaseInstance(args[0], false);

        LoginFilter testIfLoggedIn = new LoginFilter(database);
        GetFileFromDBHandler getCrawledFile = new GetFileFromDBHandler(database);
        GetIndexHandler getIndexHandler = new GetIndexHandler(database);
        

        if (args.length == 2) {
            staticFiles.externalLocation(args[1]);
            staticFileLocation(args[1]);
        }

        logger.info("Registering routes and handlers");
        before("/*", testIfLoggedIn);
        //before(loginTestGet);
        // TODO:  add /register, /logout, /index.html, /, /lookup
        get("/lookup", getCrawledFile);
        get("/index.html", getIndexHandler);
        get("/", getIndexHandler);
        //get("*", new PageNotFoundHandler());
        post("/index.html", getIndexHandler);
        post("/register", new RegistrationHandler(database));
        post("/login", new LoginHandler(database));
        get("/logout", new LogoutHandler(database));
        get("/create/:name", new CreateChannelHandler(database));
        get("/show", new GetChannelHandler(database));
        //post("/logout?", new LogoutHandler(database));
        
        awaitInitialization();
        System.out.println("Waiting to take requests");
        logger.info("Waiting to take requests");
    }
}
