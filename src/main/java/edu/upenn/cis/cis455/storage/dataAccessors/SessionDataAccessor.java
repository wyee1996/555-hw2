package edu.upenn.cis.cis455.storage.dataAccessors;

import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;

import com.sleepycat.je.DatabaseException;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;

import edu.upenn.cis.cis455.storage.DatabaseEnvironment;
import edu.upenn.cis.cis455.storage.databaseDAOs.SessionDAO;

public class SessionDataAccessor {

	// Session accessor
	private PrimaryIndex<String,SessionDAO> sessionBySessionId;
	
	public SessionDataAccessor(EntityStore store) {
		// primary key for session class
		sessionBySessionId = store.getPrimaryIndex(String.class, SessionDAO.class);
	}
	
	/**
	 * Utility function to generate a new session Id
	 * @return
	 */
	public String createSessionId() {
		String nums = "0123456789";
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 15; i++) {
			int idx = (int)(nums.length()*Math.random());
			sb.append(nums.charAt(idx));
		}
		return sb.toString();
	}
	
	/**
	 * Utility function to create a new session and save it to the database
	 * @return
	 */
	public SessionDAO createNewSession(String sessionId) throws DatabaseException {
		
		SessionDAO newSession = new SessionDAO();
		newSession.setSessionId(sessionId);
		newSession.setCreationTime(new Date());
		newSession.setExpiresTime(DateUtils.addMinutes(newSession.getCreationTime(), 5));
		
		sessionBySessionId.put(newSession);
		return newSession;
	}
	
	public void addNewSession(String sessionId) {
		SessionDAO session = createNewSession(sessionId);
		sessionBySessionId.put(session);
	}
	
	/**
	 * Utility function to delete a session from the DB given the sessionId
	 * @param sessionId
	 */
	public void deleteSession(String sessionId) throws DatabaseException {
		sessionBySessionId.delete(sessionId);
	}
	
	public SessionDAO getSessionBySessionId(String sessionId) throws DatabaseException {
		return sessionBySessionId.get(sessionId);
	}
	
	public void updateSession(SessionDAO session) throws DatabaseException {
		sessionBySessionId.put(session);
	}
	
}
